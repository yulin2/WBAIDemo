//
//  SenseArFilter.h
//  SenseAr
//

#import <Foundation/Foundation.h>
#import "SenseArMaterialRender.h"
#import <GLKit/GLKit.h>

typedef NS_ENUM(NSUInteger, SenseArFilterType){
    FILTER_STRENGTH = 0 //滤镜效果强度, 根据实际场景调节, 取值范围[0, 1.0], 推荐使用1.0, 默认值1.0
};

@interface SenseArFilter : NSObject

/**
 创建Filter
 
 @return 成功返回filter实例, 错误则返回nil
 */
+ (SenseArFilter *)createInstance;

/**
 设置滤镜风格

 @param filterPath 输入滤镜地址
 @return 成功返回YES, 错误则返回NO
 */
- (BOOL)setStyleWithPath:(NSString *)filterPath;


/**
 设置滤镜参数
 
 @param type  滤镜参数类型
 @param value 参数取值
 @return 成功返回YES, 错误则返回NO
 */
- (BOOL)setParamWithType:(SenseArFilterType)type Value:(float)value;



/**
 对OpenGL中的纹理进行滤镜处理
 
 @param iTextureIdIn   输入纹理
 @param iWidth         图像宽度
 @param iHeight        图像高度
 @param iTextureIdOut  输出纹理
 @return 成功返回YES, 错误则返回NO
 */
- (BOOL)processFilterWithTextureIn:(GLuint)iTextureIdIn
                            Width:(int)iWidth
                           Height:(int)iHeight
                       TextureOut:(GLuint)iTextureIdOut;


/**
 对OpenGL中的纹理进行滤镜处理, 并输出buffer. 
 
 @param iTextureIdIn    输入纹理
 @param iWidth          图像宽度
 @param iHeight         图像高度
 @param iTextureIdOut   输出纹理
 @param pImageOut       输出图像
 @param iPixelFormatOut 图像输出的格式
 @return 成功返回YES, 错误则返回NO
 */
- (BOOL)processFilterAndOutputBufferWithTextureIn:(GLuint)iTextureIdIn
                                           Width:(int)iWidth
                                          Height:(int)iHeight
                                      TextureOut:(GLuint)iTextureIdOut
                                        ImageOut:(Byte *)pImageOut
                                  PixelFormatOut:(SenseArPixelFormat)iPixelFormatOut;

/**
 销毁fitler
 */
- (void)destroyInstance;
@end
