//
//  SenseArTimeReportData.h
//  SenseAr
//


#import <Foundation/Foundation.h>

@interface SenseArTimeReportData : NSObject

//人脸检测开始时间ms
@property (nonatomic, assign) long long  faceStartTime;
//人脸检测结束时间ms
@property (nonatomic, assign) long long  faceEndTime;
//美颜开始时间ms
@property (nonatomic, assign) long long  beautifyStartTime;
//美颜结束时间ms
@property (nonatomic, assign) long long  beautifyEndTime;
//手势及前背景检测开始时间ms
@property (nonatomic, assign) long long  handAndSegmentStartTime;
//手势及前背景检测结束时间ms
@property (nonatomic, assign) long long  handAndSegmentEndTime;

@end
