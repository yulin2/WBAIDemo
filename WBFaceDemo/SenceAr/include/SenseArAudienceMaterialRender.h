//
//  SenseArAudienceMaterialRender.h
//  SenseAr
//

#import <Foundation/Foundation.h>
#import "SenseArMaterialRender.h"
#import "SenseArNsAd.h"

/**
 渲染素材回调
 */
typedef void(^SenseArAudienceMaterialRenderCallBack)(SenseArNsAd *nSecAd);

@interface SenseArAudienceMaterialRender : NSObject

/**
 直播前贴片广告开始展示 , 在渲染队列回调 , 不宜做耗时操作
 */
@property (nonatomic, copy) SenseArAudienceMaterialRenderCallBack renderBegin;

/**
 直播前贴片广告结束展示 , 在渲染队列回调 , 不宜做耗时操作
 */
@property (nonatomic, copy) SenseArAudienceMaterialRenderCallBack renderEnd;


/**
 创建渲染模块
 
 @param context      渲染使用的 OpenGL 环境
 
 @return 渲染模块实例
 */
+ (SenseArAudienceMaterialRender *)instanceWithGLContext:(EAGLContext *)context;


/**
 初始化 OpenGL 资源
 */
- (void)initGLResource;


/**
 释放 OpenGL 资源
 */
- (void)releaseGLResource;



/**
 设置图像大小
 
 @param iWidth  图像宽度
 @param iHeight 图像高度
 @param iStride 图像跨度
 */
- (void)setFrameWidth:(int)iWidth height:(int)iHeight stride:(int)iStride;


/**
 渲染直播前贴片广告并根据需求可以输出渲染后的图像
 
 @param pFrameInfo    渲染素材需要的绘制信息
 @param iTextureIdIn  输入 textureID
 @param iTextureIdOut 输出 textureID
 @param iPixelFormat  输出的图像格式
 @param pImageOut     输出的图像 , 传 NULL 表示不输出图像 , 性能会更好一些 .
 
 @return 渲染的状态
 */
- (SenseArRenderStatus)renderMaterialWithFrameInfo:(Byte *)pFrameInfo
                                   frameInfoLength:(int)iLength
                                         textureIn:(GLuint)iTextureIdIn
                                        textureOut:(GLuint)iTextureIdOut
                                       pixelFormat:(SenseArPixelFormat)iPixelFormat
                                          imageOut:(Byte *)pImageOut;

/**
 * 强制停止直播前贴片广告. 例如显示礼物时停止展示直播前贴片广告或不期望展示直播前贴片广告 .
 */
- (void)forceStopNsAd;


@end
