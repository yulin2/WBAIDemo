//
//  SenseArDetectResult.h
//  SenseAr
//
//  Created by sluin on 2017/5/6.
//  Copyright © 2017年 SenseTime. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 图像格式
 */
typedef enum : NSUInteger {
    
    /**
     Y    1        8bpp ( 单通道8bit灰度像素 )
     */
    PIX_FMT_GRAY8 = 0,
    
    /**
     YUV  4:2:0   12bpp ( 3通道, 一个亮度通道, 另两个为U分量和V分量通道, 所有通道都是连续的 )
     */
    PIX_FMT_YUV420P,
    
    /**
     YUV  4:2:0   12bpp ( 2通道, 一个通道是连续的亮度通道, 另一通道为UV分量交错 )
     */
    PIX_FMT_NV12,
    
    /**
     YUV  4:2:0   12bpp ( 2通道, 一个通道是连续的亮度通道, 另一通道为VU分量交错 )
     */
    PIX_FMT_NV21,
    
    /**
     BGRA 8:8:8:8 32bpp ( 4通道32bit BGRA 像素 )
     */
    PIX_FMT_BGRA8888,
    
    /**
     BGR  8:8:8   24bpp ( 3通道24bit BGR 像素 )
     */
    PIX_FMT_BGR888,
    
    /**
     BGRA 8:8:8:8 32bpp ( 4通道32bit RGBA 像素 )
     */
    PIX_FMT_RGBA8888,
    
    /**
     RGB  8:8:8   24bpp ( 3通道24bit RGB 像素 )
     */
    PIX_FMT_RGB888
    
} SenseArPixelFormat;


/**
 人脸关键点
 */
@interface SenseArFacePoint : NSObject

@property (nonatomic , assign) CGPoint point; ///< 人脸关键点坐标
@property (nonatomic , assign) float fVisibility; ///< 关键点能见度 , 1.0 : 未被遮挡 , 0.0 : 被遮挡

@end


/**
 人脸矩形区域
 */
@interface SenseArRect : NSObject

@property (nonatomic , assign) int iLeft;   ///< 矩形最左边的坐标
@property (nonatomic , assign) int iTop;    ///< 矩形最上边的坐标
@property (nonatomic , assign) int iRight;  ///< 矩形最右边的坐标
@property (nonatomic , assign) int iBottom; ///< 矩形最下边的坐标

@end


/**
 人脸检测信息
 */
@interface SenseArResultFace : NSObject

@property (nonatomic , assign) int iFaceID; ///< 人脸ID , 跟踪丢失后重新被检测到会有一个新的ID.
@property (nonatomic , assign) uint64_t iAction; ///< 脸部动作

@property (nonatomic , assign) float fScore;        ///< 置信度
@property (nonatomic , assign) float fYaw;          ///< 水平转角,真实度量的左负右正
@property (nonatomic , assign) float fPitch;        ///< 俯仰角,真实度量的上负下正
@property (nonatomic , assign) float fRoll;         ///< 旋转角,真实度量的左负右正
@property (nonatomic , assign) float fEyeDistance;  ///< 两眼间距

@property (nonatomic , strong) SenseArRect *faceRect; ///< 代表面部矩形区域
@property (nonatomic , strong) NSArray <SenseArFacePoint *>*arrFacePoints; ///< 人脸关键点数组

@end



/**
 手部检测信息
 */
@interface SenseArResultHand : NSObject

@property (nonatomic , assign) float fScore; ///< 手置信度
@property (nonatomic , assign) uint64_t iAction; ///< 手部动作

@property (nonatomic , strong) SenseArRect *handRect; ///< 代表手部矩形
@property (nonatomic , assign) CGPoint keyPoint; ///< 手部关键点


@end



/**
 前后背景分割检测信息
 */
@interface SenseArResultSegment : NSObject

@property (nonatomic , strong) NSData *imageData; ///< 图像元数据 , 格式为 8-bit灰度图

@property (nonatomic , assign) int iWidth;  ///< 宽度(以像素为单位)
@property (nonatomic , assign) int iHeight; ///< 高度(以像素为单位)
@property (nonatomic , assign) int iStride; ///< 跨度, 即每行所占的字节数


@end



/**
 检测结果
 */
@interface SenseArDetectResult : NSObject

@property (nonatomic , strong) NSArray <SenseArResultFace *>*arrFaceResults; ///< 人脸检测信息数组
@property (nonatomic , strong) NSArray <SenseArResultHand *>*arrHandResults; ///< 手部检测信息数组
@property (nonatomic , strong) SenseArResultSegment *segmentResult; ///< 前后背景分割检测信息 , 为空时表示没有做背景分割或不成功

@end
