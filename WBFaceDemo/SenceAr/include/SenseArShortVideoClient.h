//
//  SenseArShortVideoClient.h
//  SenseAr
//
//  Created by sluin on 2017/3/1.
//  Copyright © 2017年 SenseTime. All rights reserved.
//

#import "SenseArClient.h"
#import "SenseArMaterial.h"
#import "SenseArFrameActionInfo.h"

@interface SenseArShortVideoClient : SenseArClient
/**
 录制开始
 */
- (void)recordStart;
/**
 录制结束
 */
- (void)recordStop;
/**
 录制结束后，可以获取要上传的数据包
 */
- (NSString *)getReportData;
/**
 上报小视频中的素材使用信息
 
 @param userId   用户ID
 @param videoId 平台上的视频ID
 @param videolink 平台上的视频url
 @param reportData 上报数据，从getReportData()接口中获得
 @param completion 上报结果回调
 */
- (void)reportWithUseId:(NSString *)userId
                videoID:(NSString *)videoId
              videoLink:(NSString *)videolink
             reportData:(NSString *)reportData
             completion:(void(^)(BOOL isSuccess, NSString *strMessage))completion;
@end
