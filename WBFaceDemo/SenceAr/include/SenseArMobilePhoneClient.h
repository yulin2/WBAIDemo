//
//  SenseArCameraAppClient.h
//  SenseAr
//


#import "SenseArClient.h"

@interface SenseArMobilePhoneClient : SenseArClient
/**
 录制开始
 */
- (void)recordStart;
/**
 录制结束
 */
- (void)recordStop;

/**
 开始ns前贴片广告
 
 @param clienId 用户ID
 */
- (void)startRenderPicture:(NSString *)clienId;

@end
