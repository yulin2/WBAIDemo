//
//  SenseArBroadcasterClient.h
//  SenseArMaterial
//
//  Created by sluin on 16/10/8.
//  Copyright © 2016年 SenseTime. All rights reserved.
//

#import "SenseArClient.h"


/**
 主播类型客户
 */
@interface SenseArBroadcasterClient : SenseArClient



/**
 *  当前直播中的观众数
 */
@property (nonatomic , assign) int iAudienceCount;



/**
 *  是否在直播中
 */
@property (nonatomic , assign , readonly) BOOL isBroadcasting;



/**
 直播开始

 @param strReplayURL 直播回看地址 , 没有可以传空
 */
- (void)broadcastStartWithReplayURL:(NSString *)strReplayURL;


/**
 直播停止
 */
- (void)broadcastEnd;


/**
 查询主播是否在 n 秒广告投放白名单中 . 需要在 SenseArMaterialService 正确初始化并鉴权成功后 , 同时正确设置 broadcasterClient.strID (主播ID) 才能调用.

 @param completeSuccess 查询请求成功 , isSupport 是否支持 , YES 支持 , NO 不支持
 @param completeFailure 查询请求失败 , iErrorCode 错误码 , strMessage 错误信息
 */
- (void)queryAdSupportOnSuccess:(void (^)(BOOL isSupport))completeSuccess
                      onFailure:(void (^)(int iErrorCode , NSString *strMessage))completeFailure;





@end
