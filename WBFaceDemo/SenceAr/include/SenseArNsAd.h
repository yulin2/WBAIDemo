//
//  SenseArNsAd.h
//  SenseAr
//
//  Created by sluin on 2017/6/20.
//  Copyright © 2017年 SenseTime. All rights reserved.
//

#import "SenseArMaterial.h"

@interface SenseArNsAd : SenseArMaterial <NSCoding>

@property (nonatomic , assign , readonly) int iExposureTime;

@end
