//
//  SenseArClient.h
//  SenseArMaterial
//
//  Created by sluin on 16/10/8.
//  Copyright © 2016年 SenseTime. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * 客户类型
 */
typedef enum : NSUInteger {
    
    /**
     * 主播类型客户
     */
    Broadcaster = 0,
    
    /**
     * 粉丝类型客户
     */
    Audience,
    
    /**
     * 短视频类型客户
     */
    SmallVideo,
    
    /**
     * 相机类型客户
     */
    MobilePhone
    
    
} SenseArClientType;



@interface SenseArClient : NSObject

/**
 *  客户类型
 */
@property (nonatomic , assign) SenseArClientType iClientType;


/**
 *  用户在平台上的唯一标识
 */
@property (nonatomic , copy) NSString *strID;


/**
 *  用户名
 */
@property (nonatomic , copy) NSString *strName;

/**
 *  用户生日
 */
@property (nonatomic , copy) NSString *strBirthday;

/**
 *  用户性别, 男 , 女
 */
@property (nonatomic , copy) NSString *strGender;

/**
 *  地址
 */
@property (nonatomic , copy) NSString *strAddress;


/**
 *  用户邮箱地址
 */
@property (nonatomic , copy) NSString *strEmail;

/**
 *  经度
 */
@property (nonatomic , assign) double longitude;

/**
 *  纬度
 */
@property (nonatomic , assign) double latitude;


/**
 *  关注人数
 */
@property (nonatomic , assign) int iFollowCount;

/**
 *  粉丝人数
 */
@property (nonatomic , assign) int iFansCount;


/**
 * 用户的标签或类别, 可以设置多个tag，每个tag用逗号分割
 * 比如一个有效的tags： "美女，女神，财经"
 */
@property (nonatomic , copy) NSString *strTags;

/**
 * 用户来源（可以是平台名称）
 */
@property (nonatomic , copy) NSString *strChannel;

/**
 * IDFA
 */
@property (nonatomic , copy) NSString *strIdfa;

/**
 * md5处理过的IDFA
 */
@property (nonatomic , copy) NSString *strIdfaMd5;





@end
