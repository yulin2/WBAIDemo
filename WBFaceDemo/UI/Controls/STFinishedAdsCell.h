//
//  STFinishedAdsCell.h
//  SenseArDemo
//
//  Created by sensetimesunjian on 2017/9/5.
//  Copyright © 2017年 SenseTime. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface STFinishedAdsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *adImage;
@property (weak, nonatomic) IBOutlet UILabel *adName;
@property (weak, nonatomic) IBOutlet UILabel *adTime;

@end
