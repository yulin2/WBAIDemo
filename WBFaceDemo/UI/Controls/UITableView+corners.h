//
//  UITableView+corners.h
//  cornerTableView
//
//  Created by sensetimesunjian on 2017/9/6.
//  Copyright © 2017年 sensetimesunjian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (corners)
- (void)addRoundedCornersWithRadius:(CGFloat)radius ForCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
@end
