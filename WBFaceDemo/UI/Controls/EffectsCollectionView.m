//
//  EffectsCollectionView.m
//  SenseArDemo
//
//  Created by sluin on 2017/9/5.
//  Copyright © 2017年 SenseTime. All rights reserved.
//

#import "EffectsCollectionView.h"

@implementation EffectsCollectionView


- (instancetype)initWithFrame:(CGRect)frame
{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.itemSize = CGSizeMake(75.0, 75.0);
    flowLayout.minimumLineSpacing = 5.0;
    
    self = [super initWithFrame:frame collectionViewLayout:flowLayout];
    
    if (self) {
        
        self.showsHorizontalScrollIndicator = NO;
        self.showsVerticalScrollIndicator = NO;
    }
    
    return self;
    
}




@end
