//
//  EffectsCollectionViewCell.m
//  SenseArDemo
//
//  Created by sluin on 2017/9/5.
//  Copyright © 2017年 SenseTime. All rights reserved.
//

#import "EffectsCollectionViewCell.h"

@implementation EffectsCollectionViewCellModel
@end

@interface EffectsCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *soundMark;

@property (weak, nonatomic) IBOutlet UIImageView *thumbView;

@property (weak, nonatomic) IBOutlet UIButton *btnDownload;

@property (weak, nonatomic) IBOutlet UIImageView *loadingView;

@end

@implementation EffectsCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.thumbView.layer.cornerRadius = 10.0f;
    self.layer.cornerRadius = 5.0f;
    self.layer.borderWidth = 2.0f;
    self.layer.borderColor = [UIColor clearColor].CGColor;
}



- (void)setModel:(EffectsCollectionViewCellModel *)model
{
    _model = model;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self refreshUIWithModel:model];
    });
}


- (void)setIsCustomSelected:(BOOL)isCustomSelected
{
    _isCustomSelected = isCustomSelected;
    
    static UIColor *clearColor = nil;
    static UIColor *redColor = nil;
    
    if (!clearColor) {
        
        clearColor = [UIColor clearColor];
    }
    
    if (!redColor) {
        
        redColor = [UIColor redColor];
    }
    
    self.layer.borderColor = isCustomSelected ? redColor.CGColor : clearColor.CGColor;
}

- (void)refreshUIWithModel:(EffectsCollectionViewCellModel *)model
{
    if (!model || !model.material) {
        
        _thumbView.hidden = YES;
        _loadingView.hidden = YES;
        _btnDownload.hidden = YES;
        
        return;
    }
    
    _thumbView.image = model.imageThumb;
    

    if (SPECIAL_EFFECT == model.material.iMaterialType ||
        SMALL_VIDEO_EFFECT == model.material.iMaterialType ||
        CAMERA_EFFECT == model.material.iMaterialType) {
        
        if (model.downloadStatus == notDownload) {
            
            _btnDownload.hidden = NO;
        }else{
            
            _btnDownload.hidden = YES;
        }
        
        if (model.downloadStatus == isDownloaded) {
            
            _thumbView.alpha = 1.0;
        }else{
            
            _thumbView.alpha = 0.5;
        }
        
        if (isDownloading == model.downloadStatus) {
            
            _loadingView.hidden = NO;
            [self startDownloadAnimation];
        }else{
            
            _loadingView.hidden = YES;
            [self stopDownloadAnimation];
        }
    }
}

- (void)startDownloadAnimation
{
    [self.loadingView.layer removeAnimationForKey:@"rotation"];
    
    CABasicAnimation *circleAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    circleAnimation.duration = 2.5f;
    circleAnimation.repeatCount = MAXFLOAT;
    circleAnimation.toValue = @(M_PI * 2);
    [self.loadingView.layer addAnimation:circleAnimation forKey:@"rotation"];
}

- (void)stopDownloadAnimation
{
    [self.loadingView.layer removeAnimationForKey:@"rotation"];
}

#pragma - mark -
#pragma - mark Touch Event

- (IBAction)onBtnDownload:(id)sender {
    
    if (self.delegate
        &&
        [self.delegate respondsToSelector:@selector(btnDownloadDidClickInEffectsCellWithModel:)]) {
        
        [self.delegate btnDownloadDidClickInEffectsCellWithModel:self.model];
    }
}

@end
