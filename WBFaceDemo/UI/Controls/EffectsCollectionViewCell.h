//
//  EffectsCollectionViewCell.h
//  SenseArDemo
//
//  Created by sluin on 2017/9/5.
//  Copyright © 2017年 SenseTime. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SenseArMaterial.h"
#import "STEnumDef.h"



@interface EffectsCollectionViewCellModel : NSObject

@property (nonatomic , assign) int indexOfItem;
@property (nonatomic , strong) SenseArMaterial *material;
@property (nonatomic , strong) UIImage *imageThumb;
@property (nonatomic , assign) CollectionViewCellModelDownloadStatus downloadStatus;

@end



@protocol EffectsCollectionViewCellDelegate <NSObject>

- (void)btnDownloadDidClickInEffectsCellWithModel:(EffectsCollectionViewCellModel *)model;

@end



@interface EffectsCollectionViewCell : UICollectionViewCell

@property (nonatomic , weak) id <EffectsCollectionViewCellDelegate> delegate;
@property (nonatomic , strong) EffectsCollectionViewCellModel *model;
@property (nonatomic , assign) BOOL isCustomSelected;

@end
