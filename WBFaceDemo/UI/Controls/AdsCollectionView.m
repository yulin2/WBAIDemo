//
//  AdsCollectionView.m
//  SenseArDemo
//
//  Created by sluin on 2017/9/7.
//  Copyright © 2017年 SenseTime. All rights reserved.
//

#import "AdsCollectionView.h"

@implementation AdsCollectionView

- (instancetype)initWithFrame:(CGRect)frame
{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.itemSize = CGSizeMake(90.0, 90.0);
    flowLayout.minimumLineSpacing = 20.0;
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.sectionInset = UIEdgeInsetsMake(10.0, 15.0, 10.0, 15.0);
    
    self = [super initWithFrame:frame collectionViewLayout:flowLayout];
    
    if (self) {
        
        self.showsHorizontalScrollIndicator = NO;
        self.showsVerticalScrollIndicator = NO;
    }
    
    return self;
    
}

@end
