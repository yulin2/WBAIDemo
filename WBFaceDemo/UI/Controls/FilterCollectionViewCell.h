//
//  FilterCollectionViewCell.h
//  SenseArDemo
//
//  Created by sluin on 2017/9/6.
//  Copyright © 2017年 SenseTime. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterCollectionViewCellModel : NSObject

@property (nonatomic , strong) UIImage *image;
@property (nonatomic , copy) NSString *strName;
@property (nonatomic , copy) NSString *strPath;

@property (nonatomic , assign) int indexOfItem;

@property (nonatomic , assign) BOOL isSelected;

@end

@interface FilterCollectionViewCell : UICollectionViewCell

@property (nonatomic , strong) FilterCollectionViewCellModel *model;

@property (weak, nonatomic) IBOutlet UIImageView *thumbView;
@property (weak, nonatomic) IBOutlet UILabel *lblName;

@end
