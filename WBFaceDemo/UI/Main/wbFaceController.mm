#import "wbFaceController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVFoundation.h>
#import <VideoToolbox/VideoToolbox.h>
#import <OpenGLES/ES2/glext.h>
#import <CommonCrypto/CommonDigest.h>
#import "STGLPreview.h"
#import "STMobileLog.h"
#import "STMaterialDisplayConfig.h"
#import "SenseArMemoryCache.h"
#import "STMovieRecorder.h"
#import "SenseAr.h"
#import "STUtil.h"
#import "STCamera.h"
#import "STCustomToolBar.h"
#import "EffectsCollectionView.h"
#import "EffectsCollectionViewCell.h"
#import "FilterCollectionView.h"
#import "FilterCollectionViewCell.h"
#import "AdsCollectionViewCell.h"
#import "libyuv.h"
#import "STCustomToolBar.h"
#import "bgsegment.h"
// 两种 check license 的方式 , 一种是根据 license 文件的路径 , 另一种是 license 文件的缓存选择应用场景合适的即可
#define CHECK_LICENSE_WITH_PATH 1

// 设置显示及视频编码帧率
#define kENCODE_FPS 20

// 提示动作标签停留时间
#define kACTION_TIP_STAY_TIME 2.0f

#define kRECORD_MAX_TIME 20.0f


typedef NS_ENUM(NSInteger, STWriterRecordingStatus){
    STWriterRecordingStatusIdle = 0,
    STWriterRecordingStatusStartingRecording,
    STWriterRecordingStatusRecording,
    STWriterRecordingStatusStoppingRecording
};


@interface wbFaceController () <UIAlertViewDelegate, STMovieRecorderDelegate, STCameraDelegate, EffectsCollectionViewCellDelegate>

{
    int _iImageWidth;
    int _iImageHeight;

    CVOpenGLESTextureRef _cvTextureBeautify;
    CVOpenGLESTextureRef _cvTextureBeautifySharp;
    CVOpenGLESTextureRef _cvTextureSticker;
    CVOpenGLESTextureRef _cvTextureFilter;
    
    CVPixelBufferRef _cvBeautifyBuffer;
    CVPixelBufferRef _cvBeautifyShapeBuffer;
    CVPixelBufferRef _cvStickerBuffer;
    CVPixelBufferRef _cvFilterBuffer;
    CVPixelBufferRef _ForAIInputBuffer;
    CVPixelBufferRef _ForAIOutputBuffer;


    GLuint _textureOriginalIn;
    GLuint _textureBeautifyOut;
    GLuint _textureBeautifyShapeOut;
    GLuint _textureStickerOut;
    GLuint _textureFilterOut;
    double* bytes;

}

@property (nonatomic , strong) STCamera *stCamera;

// SenseAR
@property (nonatomic) CVOpenGLESTextureRef cvOriginalTexture;
@property (nonatomic) CVOpenGLESTextureCacheRef cvTextureCache;
@property (nonatomic , strong) SenseArMaterial *currentMaterial;
@property (nonatomic , strong) SenseArMaterial *prepareMaterial; // 期望展示的素材
@property (nonatomic , strong) SenseArMaterialService *service;
@property (nonatomic , strong) SenseArShortVideoClient  *smallvideo;
@property (nonatomic , strong) SenseArMobilePhoneClient *mobilePhone;
@property (nonatomic , strong) SenseArMaterialRender *render;
@property (nonatomic , strong) SenseArFilter  *filter;
@property (nonatomic , strong) STMovieRecorder *stRecorder;
@property (nonatomic , assign) STWriterRecordingStatus recordStatus;
@property (nonatomic , assign) BOOL isRecording;
@property (nonatomic , assign) BOOL isSavesucceed;
@property (nonatomic , assign) double recordStartTime;
@property (nonatomic , strong) NSURL *recorderURL;
@property (nonatomic , assign) BOOL bSticker;

@property (nonatomic) dispatch_queue_t callBackQueue;
@property (nonatomic) dispatch_queue_t thumbDownlaodQueue;
@property (nonatomic, strong) NSOperationQueue *imageLoadQueue;

@property (nonatomic, strong) __attribute__((NSObject)) CMFormatDescriptionRef outputVideoFormatDescription;
@property (nonatomic, strong) __attribute__((NSObject)) CMFormatDescriptionRef outputAudioFormatDescription;
@property (nonatomic, strong) NSDictionary *videoCompressingSettings;
@property (nonatomic, strong) NSDictionary *audioCompressingSettings;
@property (nonatomic , strong) STGLPreview *glPreview;
@property (nonatomic , strong) EAGLContext *glRenderContext;


// ad list
@property (nonatomic , assign) BOOL isCheckingMaterial;
@property (nonatomic , strong) NSMutableArray *arrStickers;
@property (nonatomic , assign) BOOL isAppActive;

@property (nonatomic , strong) SenseArMemoryCache *thumbnailCache;
@property (nonatomic , strong) SenseArMemoryCache *stickerListMap;

@property (nonatomic , copy) NSString *strThumbnailPath;

@property (nonatomic , strong) NSFileManager *fManager;
@property (strong, nonatomic)  UIImageView *actionTipImageView;
@property (strong, nonatomic)  UILabel *lblActionTip;
@property (nonatomic , assign) double dLastActionTipPTS;

@property (nonatomic , strong) NSDictionary *dicMaterialDisplayConfig;
@property (nonatomic , strong) NSArray *arrLastMaterialParts;

@property (nonatomic , copy) NSString *strLastMaterialID;
@property (nonatomic , assign) BOOL isLastFrameTriggered;

@property (nonatomic , strong) STCustomToolBar *effectBar;
@property (nonatomic , strong) STCustomToolBar *beautifyBar;
@property (nonatomic , strong) STCustomToolBar *bottomBar;

@property (nonatomic , strong) UIView *effectMenu;
@property (nonatomic , strong) UIView *beautifyMenu;

// 特效列表
@property (nonatomic , strong) SenseArMemoryCache *effectsDataSource;
@property (nonatomic , strong) EffectsCollectionView *effectsList;
@property (nonatomic , strong) EffectsCollectionViewCellModel *selectedEffectsModel;


@property (nonatomic , assign) float fSmoothStrength;
@property (nonatomic , assign) float fWhitenStrength;
@property (nonatomic , assign) float fReddenStrength;
@property (nonatomic , assign) float fShrinkFaceRatio;
@property (nonatomic , assign) float fEnlargeEyeRatio;
@property (nonatomic , assign) float fShrinkJawRatio;

@property (nonatomic , strong) UIView *beautifyBGView;

@property (nonatomic , strong) UIImageView *beautifyThumb1;
@property (nonatomic , strong) UIImageView *beautifyThumb2;
@property (nonatomic , strong) UIImageView *beautifyThumb3;

@property (nonatomic , strong) UILabel *lblBeautfiyDesc1;
@property (nonatomic , strong) UILabel *lblBeautfiyDesc2;
@property (nonatomic , strong) UILabel *lblBeautfiyDesc3;

@property (nonatomic , strong) UISlider *beautifySlider1;
@property (nonatomic , strong) UISlider *beautifySlider2;
@property (nonatomic , strong) UISlider *beautifySlider3;

//smallvideo UI
@property (weak, nonatomic) IBOutlet UIButton *btnCamera;
@property (nonatomic , strong) UIButton *btnRecord;
@property (nonatomic , strong) CAShapeLayer * shapeLayer;
@property (nonatomic , strong) UIButton *btnSave;
@property (nonatomic , strong) UIButton *btnSaveSecceed;
@property (nonatomic , strong) UIButton *btnCancel;


@property (nonatomic , strong) UILabel *lbl100;
@property (nonatomic , strong) UILabel *lbl101;
@property (nonatomic , strong) UILabel *lbl102;

@end

@implementation wbFaceController

- (BOOL)prefersStatusBarHidden
{
    return YES; //返回NO表示要显示，返回YES将hiden
}

- (void)appWillResignActive
{
    self.isAppActive = NO;

    if (self.isRecording) {

        [self onBtnRecord:nil];
    }
}
- (void)applicationDidEnterBackground
{
    self.isAppActive = NO;
    
    if (self.isRecording) {
        
        [self onBtnRecord:nil];
    }
}

- (void)appWillEnterForeground
{
    self.isAppActive = YES;
}

- (void)appDidBecomeActive
{
    self.isAppActive = YES;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self addNotifications];
    
    [self setDefaultValue];
    
    // 设置缩略图缓存
    [self setupThumbnailCache];
    
    //设置相机
    [self setupCamera];
    
    // 设置预览
    [self setupPreview];
    
    // 验证 license
    [self checkActiveCode];
    
    // 设置并开启 SenseArService
    [self setupSenseArServiceAndSmallVideo];
    
    // 设置渲染模块 , 美颜参数
    [self setupMaterialRender];
    
    _recorderURL = [[NSURL alloc] initFileURLWithPath:[NSString pathWithComponents:@[NSTemporaryDirectory(), @"Movie.MOV"]]];
    
    [self setupLayout];
    
    //配置滤镜
    [self setupFilter];
    
    //配置AIBuffer
    [self createAIBuffer];
    
    [self.stCamera startRunning];
    
    
}
- (void)addNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appWillResignActive)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidEnterBackground) name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appWillEnterForeground)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appDidBecomeActive) name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
}
- (void)setDefaultValue
{
    self.thumbDownlaodQueue = dispatch_queue_create("com.sensetime.sensear.thumbDownloadQueue", NULL);
    self.imageLoadQueue = [[NSOperationQueue alloc] init];
    self.imageLoadQueue.maxConcurrentOperationCount = 20;
    
    self.isSavesucceed = NO;
    
    self.dLastActionTipPTS = 0.0;
    self.isAppActive = YES;
    self.isLastFrameTriggered = NO;
    self.recordStatus = STWriterRecordingStatusIdle;
    self.bSticker = NO;
    self.arrStickers = [NSMutableArray array];
    
    self.stickerListMap = [[SenseArMemoryCache alloc] init];
    
    // 根据 AVCaptureSession 设置的实际 sessionPreset 来设置宽高 , 因为使用了旋转 , 所以宽是短边 .
    _iImageWidth = 720;
    _iImageHeight = 1280;
    
    
    self.fReddenStrength = 0.36;
    self.fSmoothStrength = 0.74;
    self.fWhitenStrength = 0.02;
    self.fShrinkFaceRatio = 0.11;
    self.fEnlargeEyeRatio = 0.13;
    self.fShrinkJawRatio = 0.10;
    
    self.isRecording = NO;
    
    
    // 默认的素材展示序列及子素材展示配置
    STMaterialDisplayConfig *config1 = [[STMaterialDisplayConfig alloc] init];
    config1.iTriggerType = SENSEAR_HAND_LOVE;
    config1.arrMaterialPartsSequence = @[@[@"ear", @"face", @"pink"],
                                         @[@"ear", @"face", @"yellow"],
                                         @[@"ear", @"face", @"purple"]];
    
    STMaterialDisplayConfig *config2 = [[STMaterialDisplayConfig alloc] init];
    config2.iTriggerType = SENSEAR_HAND_PALM;
    config2.arrMaterialPartsSequence = @[@[@"head", @"face", @"cocacolab"],
                                         @[@"head", @"face", @"jdba"],
                                         @[@"head", @"face", @"milk"]];
    
    self.dicMaterialDisplayConfig = @{
                                      @"20170109124245233850861" : config1 ,
                                      @"20170109124355279333705" : config2
                                      };
}
- (void)setupFilter
{
    //实例化滤镜
    self.filter = [SenseArFilter createInstance];
    [self.filter setParamWithType:FILTER_STRENGTH Value:0.5];
}

- (void)setupThumbnailCache
{
    self.thumbnailCache = [[SenseArMemoryCache alloc] init];
    self.fManager = [[NSFileManager alloc] init];
    
    // 可以根据实际情况实现素材列表缩略图的缓存策略 , 这里仅做演示 .
    self.strThumbnailPath = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:@"sensear_thumbnail"];
    
    NSError *error = nil;
    BOOL bCreateSucceed = [self.fManager createDirectoryAtPath:self.strThumbnailPath
                                   withIntermediateDirectories:YES
                                                    attributes:nil
                                                         error:&error];
    if (!bCreateSucceed || error) {
        
        STLog(@"create thumbnail cache directory failed !");
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"创建列表图片缓存文件夹失败" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
        
        [alert show];
    }
}

- (void)fetchList
{
    [self fetchMaterialsAndReloadDataWithGroupID:@"1fd98ab06efa11e8a78502f2bed8b8db" typeIndex:0];
    [self fetchMaterialsAndReloadDataWithGroupID:@"a3917df0743511e8b55d02f2be10f737" typeIndex:1];
   // [self fetchMaterialsAndReloadDataWithGroupID:@"19f00ed08d3711e7a7f100163e0e6edd" typeIndex:1];
   // [self fetchMaterialsAndReloadDataWithGroupID:@"2f01bc108d3711e7a7f100163e0e6edd" typeIndex:2];
   // [self fetchMaterialsAndReloadDataWithGroupID:@"38a478708d3711e7a7f100163e0e6edd" typeIndex:3];
   // [self fetchMaterialsAndReloadDataWithGroupID:@"5104db808d3711e7a7f100163e0e6edd" typeIndex:3];
//    [self.effectsDataSource setObject:[NSArray array]                                 forKey:@(4)];
   // [self fetchMaterialsAndReloadDataWithGroupID:@"646249108d3711e7a7f100163e0e6edd" typeIndex:4];
}

- (void)fetchMaterialsAndReloadDataWithGroupID:(NSString *)strGroupID
                                     typeIndex:(int)iIndex
{
    __weak typeof(self) weakSelf = self;
    
    [self.service fetchMaterialsWithUserID:@"weibo"
                                   GroupID:strGroupID
                                    adMode:SMALL_VIDEO_EFFECT
                                 onSuccess:^(NSArray<SenseArMaterial *> *arrMaterials) {
                                     NSMutableArray *arrModels = [NSMutableArray array];
                                     
                                     for (int i = 0; i < arrMaterials.count; i ++) {
                                         
                                         SenseArMaterial *material = [arrMaterials objectAtIndex:i];
                                         
                                         EffectsCollectionViewCellModel *model = [[EffectsCollectionViewCellModel alloc] init];
                                         
                                        
                                         if(iIndex == 0){
                                             if (i == arrMaterials.count -1){
                                                 [material setValue:@"xiaomifeng" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -2){
                                                 [material setValue:@"ali" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -3){
                                                 [material setValue:@"bandiangouzi" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -4){
                                                 [material setValue:@"ama" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -5){
                                                 [material setValue:@"chicken" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -6){
                                                 [material setValue:@"chunwan" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -7){
                                                 [material setValue:@"dangaotuzi" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -8){
                                                 [material setValue:@"unicorn" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -9){
                                                 [material setValue:@"huluwa" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -10){
                                                 [material setValue:@"blackcat" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -11){
                                                 [material setValue:@"huanghuahua" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -12){
                                                 [material setValue:@"jianzhimao" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -13){
                                                 [material setValue:@"jp" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -14){
                                                 [material setValue:@"lichen" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -15){
                                                 [material setValue:@"lizihuangguan" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -16){
                                                 [material setValue:@"lianjia" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -17){
                                                 [material setValue:@"benpaoluhan" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -18){
                                                 [material setValue:@"catbeard" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -19){
                                                 [material setValue:@"daerduo" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -20){
                                                 [material setValue:@"chenhe" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -21){
                                                 [material setValue:@"liebao" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -22){
                                                 [material setValue:@"redface" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -23){
                                                 [material setValue:@"shengdanlu" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -24){
                                                 [material setValue:@"taoxin" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -25){
                                                 [material setValue:@"tuziglasses" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -26){
                                                 [material setValue:@"wangzhulan" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -27){
                                                 [material setValue:@"xiquqiang" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -28){
                                                 [material setValue:@"PNG" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -29){
                                                 [material setValue:@"xiaoxiongmao" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -30){
                                                 [material setValue:@"yanzhihuahuan" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -31){
                                                 [material setValue:@"baby" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -32){
                                                 [material setValue:@"zhangcao" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -33){
                                                 [material setValue:@"paonanangelbaby" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -34){
                                                 [material setValue:@"lemon" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -35){
                                                 [material setValue:@"thuglife" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -36){
                                                 [material setValue:@"woaini" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -37){
                                                 [material setValue:@"huzi" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -38){
                                                 [material setValue:@"erduo" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -39){
                                                 [material setValue:@"chihuojianfei" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -40){
                                                 [material setValue:@"meditation" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -41){
                                                 [material setValue:@"hanleng" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -42){
                                                 [material setValue:@"huahuanerduo" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -43){
                                                 [material setValue:@"leisimaoer" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -44){
                                                 [material setValue:@"lichun" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -45){
                                                 [material setValue:@"huangguan" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -46){
                                                 [material setValue:@"nsjwg" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -47){
                                                 [material setValue:@"qinqinmao" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -48){
                                                 [material setValue:@"qinghuaci" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -49){
                                                 [material setValue:@"hongheituer" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -50){
                                                 [material setValue:@"shishangbeijin" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -51){
                                                 [material setValue:@"taoxin2" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -52){
                                                 [material setValue:@"wanzhitou" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -53){
                                                 [material setValue:@"xiangshuhuabiankuang" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -54){
                                                 [material setValue:@"smallplait" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -55){
                                                 [material setValue:@"xiaofengche" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -56){
                                                 [material setValue:@"xiaonian" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -57){
                                                 [material setValue:@"xingkongxuanzhuan" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -58){
                                                 [material setValue:@"study" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -59){
                                                 [material setValue:@"fashion" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -60){
                                                 [material setValue:@"yanjing" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -61){
                                                 [material setValue:@"hipop" forKey:@"strName"];
                                             }
                                         }else if(iIndex == 1){
                                             if (i == arrMaterials.count -1){
                                                 [material setValue:@"cty" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -2){
                                                 [material setValue:@"baobao" forKey:@"strName"];
                                             }else if(i == arrMaterials.count -3){
                                                 [material setValue:@"zhaocaimao" forKey:@"strName"];
                                             }
                                         }
                                       
                                         
                                        
                                         [self copyFile2Documents:material.strName];
                                         
                    
                                         model.material = material;
                                         model.indexOfItem = i;
                                         
                                         [arrModels addObject:model];
                                     }
                                     
                                     [weakSelf.effectsDataSource setObject:arrModels forKey:@(iIndex)];
                                     
                                     if (weakSelf.effectBar.iSelectedIndex == iIndex) {
                                         
                                         [weakSelf.effectsList reloadData];
                                     }
                                     
                                     for (EffectsCollectionViewCellModel *model in arrModels) {
                                         
                                         dispatch_async(weakSelf.thumbDownlaodQueue, ^{
                                             
                                             [weakSelf cacheThumbnailOfModel:model];
                                         });
                                     }
                                 } onFailure:^(int iErrorCode, NSString *strMessage) {
                                     
                                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil];
                                     
                                     [alert setMessage:[NSString stringWithFormat:@"获取贴纸列表失败 , %@" , strMessage]];
                                     
                                     [alert show];
                                 }];
}


- (void)setupLayout
{
    self.effectsDataSource = [[SenseArMemoryCache alloc] init];
    
    CGRect menuRect = CGRectMake(0, SCREEN_HEIGHT - 65.0 - 290.0, SCREEN_WIDTH, 290);
    UIColor *menuColor = [UIColor colorWithRed:22.0 / 255.0 green:24.0 / 255.0 blue:31.0 / 255.0 alpha:0.93];
    
    self.effectMenu = [[UIView alloc] initWithFrame:menuRect];
    self.effectMenu.backgroundColor = menuColor;
    self.effectMenu.userInteractionEnabled = YES;
    self.effectMenu.hidden = YES;
    [self.view addSubview:self.effectMenu];
    
    self.beautifyMenu = [[UIView alloc] initWithFrame:menuRect];
    self.beautifyMenu.backgroundColor = menuColor;
    self.beautifyMenu.hidden = YES;
    [self.view addSubview:self.beautifyMenu];
    
    
    __weak typeof(self) weakSelf = self;
    
    UIView *btnNullBGView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 45.0, 45.0)];
    btnNullBGView.backgroundColor = UIColorFromRGB(0x0f131c);
    [self.effectMenu addSubview:btnNullBGView];
    
    UIButton *btnNull = [UIButton buttonWithType:UIButtonTypeCustom];
    btnNull.frame = CGRectMake(0, 2, 41.0, 41.0);
    [btnNull setBackgroundImage:[UIImage imageNamed:@"无"] forState:UIControlStateNormal];
    btnNull.imageView.contentMode = UIViewContentModeScaleAspectFit;
    btnNull.backgroundColor = UIColorFromRGB(0x0f131c);
    [btnNull addTarget:self action:@selector(onBtnNull) forControlEvents:UIControlEventTouchUpInside];
    [btnNullBGView addSubview:btnNull];
    
    NSArray *arrEffectItems = @[createCustomToolBarItem(@"2d",
                                                       nil,
                                                        nil),
                                createCustomToolBarItem(@"动作",
                                                        nil,
                                                        nil)
//                                createCustomToolBarItem(nil,
//                                                        [UIImage imageNamed:@"3d"],
//                                                        [UIImage imageNamed:@"3d选中"]),
//                                createCustomToolBarItem(nil,
//                                                        [UIImage imageNamed:@"手势"],
//                                                        [UIImage imageNamed:@"手势-选中"]),
//                                createCustomToolBarItem(nil,
//                                                        [UIImage imageNamed:@"物体追踪"],
//                                                        [UIImage imageNamed:@"物体追踪选中"]),
//                                createCustomToolBarItem(nil,
//                                                        [UIImage imageNamed:@"变脸"],
//                                                        [UIImage imageNamed:@"变脸选中"])
                                ];
    
    self.effectBar = [[STCustomToolBar alloc] initWithFrame:CGRectMake(45.0,
                                                                       0,
                                                                       SCREEN_WIDTH,
                                                                       45.0)
                                              scrollEnabled:YES
                                                 equalParts:NO
                                      firstItemLeadingSpace:10
                                                    spacing:20
                                           highlightedColor:[UIColor redColor]
                                              markViewStyle:STCustomToolBarDottedMark items:arrEffectItems
                                             selectCallback:^(int iSelectedIndex)
                      {
                          [weakSelf.effectsList reloadData];
                      }];
    
    [self.effectMenu addSubview:self.effectBar];
    
    NSArray *arrBeautifyItems = @[
                                  createCustomToolBarItem(@"基础美颜", nil, nil),
                                  createCustomToolBarItem(@"美形", nil, nil)
                                  ];
    
    self.beautifyBar = [[STCustomToolBar alloc] initWithFrame:CGRectMake(0,
                                                                        0,
                                                                        SCREEN_WIDTH,
                                                                        45.0)
                                               scrollEnabled:NO
                                                  equalParts:NO
                                       firstItemLeadingSpace:10
                                                     spacing:0
                                             highlightedColor:[UIColor redColor]
                                               markViewStyle:STCustomToolBarDottedMark items:arrBeautifyItems
                                              selectCallback:^(int iSelectedIndex)
                       {
                           [weakSelf showDetailWithBeautifyBarItemIndex:iSelectedIndex];
                       }];
    
    [self.beautifyMenu addSubview:self.beautifyBar];
    
    NSArray *arrBottmBarItems = @[
                                  createCustomToolBarItem(@"特效", nil, nil),
                                  createCustomToolBarItem(@"美颜", nil, nil)
                                  ];
    
    self.bottomBar = [[STCustomToolBar alloc] initWithFrame:CGRectMake(0,
                                                                       SCREEN_HEIGHT - 65.0,
                                                                       SCREEN_WIDTH,
                                                                       65.0)
                                              scrollEnabled:NO
                                                 equalParts:YES
                                      firstItemLeadingSpace:0
                                                    spacing:0
                                           highlightedColor:[UIColor whiteColor]
                                              markViewStyle:STCustomToolBarSquareMark
                                                      items:arrBottmBarItems
                                             selectCallback:^(int iSelectedIndex)
                      {
                           [weakSelf showMenuWithBottomItemIndex:iSelectedIndex];
                      }];
    
    [self.view addSubview:self.bottomBar];
    
    CGRect listRect = CGRectMake(0, 45.0, SCREEN_WIDTH, CGRectGetHeight(menuRect) - 45.0);
    
    self.effectsList = [[EffectsCollectionView alloc] initWithFrame:listRect];
    
    [self.effectsList registerNib:[UINib nibWithNibName:@"EffectsCollectionViewCell"
                                                 bundle:[NSBundle mainBundle]]
       forCellWithReuseIdentifier:@"EffectsCollectionViewCell"];
    self.effectsList.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    [self.effectMenu addSubview:self.effectsList];
    
    self.effectsList.numberOfSectionsInView = ^NSInteger(STCustomCollectionView *collectionView) {
        
        return 1;
    };
    self.effectsList.numberOfItemsInSection = ^NSInteger(STCustomCollectionView *collectionView, NSInteger section) {
        
        NSArray *arrModels = [weakSelf.effectsDataSource objectForKey:@(weakSelf.effectBar.iSelectedIndex)];
        
        return arrModels.count;
    };
    self.effectsList.cellForItemAtIndexPath = ^UICollectionViewCell *(STCustomCollectionView *collectionView, NSIndexPath *indexPath) {
        
        static NSString *strIdentifier = @"EffectsCollectionViewCell";
        
        EffectsCollectionViewCell *cell = (EffectsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:strIdentifier forIndexPath:indexPath];
        
        cell.delegate = weakSelf;
        
        NSArray *arrModels = [weakSelf.effectsDataSource objectForKey:@(weakSelf.effectBar.iSelectedIndex)];
        
        int iModelsCount = (int)arrModels.count;
        
        if (iModelsCount) {
            
            EffectsCollectionViewCellModel *model = arrModels[indexPath.item];
            
            cell.isCustomSelected = (model == weakSelf.selectedEffectsModel) || [model.material.strID isEqualToString:weakSelf.selectedEffectsModel.material.strID];
            
            id cacheObj = [weakSelf.thumbnailCache objectForKey:model.material.strMaterialFileID];
            
            if (cacheObj && [cacheObj isKindOfClass:[UIImage class]]) {
                
                model.imageThumb = cacheObj;
            }else{
                
                model.imageThumb = nil;
            }
            
            if (   model.downloadStatus != isDownloading
                && ![weakSelf.service isMaterialDownloaded:model.material]) {
                
                model.downloadStatus = notDownload;
            }
            
            if ([weakSelf.service isMaterialDownloaded:model.material]) {
                
                model.downloadStatus = isDownloaded;
            }
            
            cell.model = model;
            
            return cell;
        }else{
            
            cell.model = nil;
            
            return cell;
        }
    };
    self.effectsList.didSelectItematIndexPath = ^(STCustomCollectionView *collectionView, NSIndexPath *indexPath) {
        
        EffectsCollectionViewCellModel *model = [weakSelf.effectsDataSource objectForKey:@(weakSelf.effectBar.iSelectedIndex)][indexPath.item];
        
        [weakSelf listDidSelectWithModel:model];
    };
    
    

    self.beautifyBGView = [[UIView alloc] initWithFrame:self.effectsList.frame];
    self.beautifyBGView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3f];
    [self.beautifyMenu addSubview:self.beautifyBGView];
    
    CGFloat fOffsetY = CGRectGetHeight(self.beautifyBGView.frame) / 4.0f;
    CGFloat fSpacing = 3.0f;
    
    self.beautifyThumb1 = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, fOffsetY - 18.0, 36.0, 36.0)];
    [self.beautifyBGView addSubview:self.beautifyThumb1];
    
    self.beautifyThumb2 = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, fOffsetY * 2.0 - 18.0, 36.0, 36.0)];
    [self.beautifyBGView addSubview:self.beautifyThumb2];
    
    self.beautifyThumb3 = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, fOffsetY * 3.0 - 18.0, 36.0, 36.0)];
    [self.beautifyBGView addSubview:self.beautifyThumb3];
    
    self.lblBeautfiyDesc1 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.beautifyThumb1.frame) + fSpacing, fOffsetY - 20.0, 40.0, 40.0)];
    self.lblBeautfiyDesc1.textColor = [UIColor whiteColor];
    self.lblBeautfiyDesc1.textAlignment = NSTextAlignmentCenter;
    self.lblBeautfiyDesc1.font = [UIFont boldSystemFontOfSize:15.0];
    [self.beautifyBGView addSubview:self.lblBeautfiyDesc1];
    
    self.lblBeautfiyDesc2 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.lblBeautfiyDesc1.frame), fOffsetY * 2.0 - 20.0, 40.0, 40.0)];
    self.lblBeautfiyDesc2.textColor = [UIColor whiteColor];
    self.lblBeautfiyDesc2.textAlignment = NSTextAlignmentCenter;
    self.lblBeautfiyDesc2.font = [UIFont boldSystemFontOfSize:15.0];
    [self.beautifyBGView addSubview:self.lblBeautfiyDesc2];
    
    self.lblBeautfiyDesc3 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.lblBeautfiyDesc1.frame), fOffsetY * 3.0 - 20.0, 40.0, 40.0)];
    self.lblBeautfiyDesc3.textColor = [UIColor whiteColor];
    self.lblBeautfiyDesc3.textAlignment = NSTextAlignmentCenter;
    self.lblBeautfiyDesc3.font = [UIFont boldSystemFontOfSize:15.0];
    [self.beautifyBGView addSubview:self.lblBeautfiyDesc3];
    
    self.lblActionTip = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100.0, 21.0)];
    self.lblActionTip.center = CGPointMake(SCREEN_WIDTH / 2.0, SCREEN_HEIGHT / 2.0);
    self.lblActionTip.adjustsFontSizeToFitWidth = YES;
    self.lblActionTip.textColor = [UIColor whiteColor];
    self.lblActionTip.textAlignment = NSTextAlignmentCenter;
    self.lblActionTip.hidden = YES;
    [self.view addSubview:self.lblActionTip];
    
    self.actionTipImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.lblActionTip.frame) - 30.0, self.lblActionTip.center.y - 15.0, 30.0, 30.0)];
    self.actionTipImageView.hidden = YES;
    [self.view addSubview:self.actionTipImageView];
    
    UILabel *lbl00 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.lblBeautfiyDesc1.frame) + fSpacing, fOffsetY - 10.0, 10.0, 20.0)];
    lbl00.textColor = [UIColor whiteColor];
    lbl00.textAlignment = NSTextAlignmentCenter;
    lbl00.font = [UIFont systemFontOfSize:15.0];
    lbl00.text = @"0";
    [self.beautifyBGView addSubview:lbl00];
    
    UILabel *lbl01 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(lbl00.frame), fOffsetY * 2.0 - 10.0, 10.0, 20.0)];
    lbl01.textColor = [UIColor whiteColor];
    lbl01.textAlignment = NSTextAlignmentCenter;
    lbl01.font = [UIFont systemFontOfSize:15.0];
    lbl01.text = @"0";
    [self.beautifyBGView addSubview:lbl01];
    
    UILabel *lbl02 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(lbl00.frame), fOffsetY * 3.0 - 10.0, 10.0, 20.0)];
    lbl02.textColor = [UIColor whiteColor];
    lbl02.textAlignment = NSTextAlignmentCenter;
    lbl02.font = [UIFont systemFontOfSize:15.0];
    lbl02.text = @"0";
    [self.beautifyBGView addSubview:lbl02];
    
    self.lbl100 = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 10.0 - 20.0, fOffsetY - 10.0, 30.0, 20.0)];
    self.lbl100.textColor = [UIColor whiteColor];
    self.lbl100.textAlignment = NSTextAlignmentCenter;
    self.lbl100.font = [UIFont systemFontOfSize:15.0];
    [self.beautifyBGView addSubview:self.lbl100];
    
    self.lbl101 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.lbl100.frame), fOffsetY * 2.0 - 10.0, 30.0, 20.0)];
    self.lbl101.textColor = [UIColor whiteColor];
    self.lbl101.textAlignment = NSTextAlignmentCenter;
    self.lbl101.font = [UIFont systemFontOfSize:15.0];
    [self.beautifyBGView addSubview:self.lbl101];
    
    self.lbl102 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.lbl100.frame), fOffsetY * 3.0 - 10.0, 30.0, 20.0)];
    self.lbl102.textColor = [UIColor whiteColor];
    self.lbl102.textAlignment = NSTextAlignmentCenter;
    self.lbl102.font = [UIFont systemFontOfSize:15.0];
    [self.beautifyBGView addSubview:self.lbl102];
    
    CGFloat fSliderWidth = SCREEN_WIDTH - 10.0 * 2 - fSpacing * 4 - 35.0 - 40.0 - 10.0 - 30.0;
    CGFloat fCenterX = CGRectGetMaxX(lbl00.frame) + 5.0 + fSliderWidth / 2.0;
    
    self.beautifySlider1 = [[UISlider alloc] initWithFrame:CGRectMake(0, 0, fSliderWidth, 10.0)];
    self.beautifySlider1.center = CGPointMake(fCenterX, fOffsetY);
    self.beautifySlider1.minimumTrackTintColor = [UIColor whiteColor];
    self.beautifySlider1.maximumTrackTintColor = [UIColor lightGrayColor];
    self.beautifySlider1.maximumValue = 1.0;
    self.beautifySlider1.tag = 100;
    [self.beautifySlider1 addTarget:self action:@selector(onBeautifySliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.beautifyBGView addSubview:self.beautifySlider1];
    
    self.beautifySlider2 = [[UISlider alloc] initWithFrame:CGRectMake(0, 0, fSliderWidth, 10.0)];
    self.beautifySlider2.center = CGPointMake(fCenterX, fOffsetY * 2);
    self.beautifySlider2.minimumTrackTintColor = [UIColor whiteColor];
    self.beautifySlider2.maximumTrackTintColor = [UIColor lightGrayColor];
    self.beautifySlider2.maximumValue = 1.0;
    self.beautifySlider2.tag = 101;
    [self.beautifySlider2 addTarget:self action:@selector(onBeautifySliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.beautifyBGView addSubview:self.beautifySlider2];
    
    self.beautifySlider3 = [[UISlider alloc] initWithFrame:CGRectMake(0, 0, fSliderWidth, 10.0)];
    self.beautifySlider3.center = CGPointMake(fCenterX, fOffsetY * 3);
    self.beautifySlider3.minimumTrackTintColor = [UIColor whiteColor];
    self.beautifySlider3.maximumTrackTintColor = [UIColor lightGrayColor];
    self.beautifySlider3.maximumValue = 1.0;
    self.beautifySlider3.tag = 102;
    [self.beautifySlider3 addTarget:self action:@selector(onBeautifySliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.beautifyBGView addSubview:self.beautifySlider3];
    
    
    self.btnCamera = [self addBtnToViewImage:@"切换前后摄像头"
                                    isHidden:NO
                                    selector:@selector(onBtnCamera:)
                                       frame:CGRectMake(SCREEN_WIDTH - 38, 10, 28, 23)];
    
    self.btnRecord = [self addBtnToViewImage:@"相机-拍照-拷贝"
                                  isHidden:NO
                                  selector:@selector(onBtnRecord:)
                                     frame:CGRectMake(SCREEN_WIDTH/2 - BTNW/2 , SCREEN_HEIGHT - BTNW - BTNBOTTOM, BTNW, BTNW)];
    
    self.btnCancel = [self addBtnToViewImage:@"取消"
                                    isHidden:YES
                                    selector:@selector(onBtnCancel:)
                                       frame:CGRectMake(BTNLEFT, CGRectGetMinY(self.btnRecord.frame), BTNW, BTNW)];
    
    self.btnSave = [self addBtnToViewImage:@"保存"
                                  isHidden:YES
                                  selector:@selector(onBtnSave:)
                                     frame:CGRectMake(SCREEN_WIDTH - BTNW - BTNLEFT, CGRectGetMinY(self.btnRecord.frame), BTNW, BTNW)];
    
    self.btnSaveSecceed = [self addBtnToViewImage:@"保存成功"
                                         isHidden:YES
                                         selector:@selector(onBtnSaveSucceed:)
                                            frame:self.btnRecord.frame];
}

- (UIButton *)addBtnToViewImage:(NSString *)imageName
                       isHidden:(BOOL)hidden
                       selector:(SEL)action
                          frame:(CGRect)rect
{
    UIImage *image = [UIImage imageNamed:imageName];
    UIButton *btn = [[UIButton alloc] initWithFrame:rect];
    [self.view addSubview:btn];
    [btn setBackgroundImage:image forState:UIControlStateNormal];
    btn.hidden = hidden;
    [btn addTarget:self
            action:action
  forControlEvents:UIControlEventTouchUpInside];
    return btn;
}

#pragma - mark -
#pragma - mark Touch Event

- (void)onBeautifySliderValueChanged:(UISlider *)slider
{
    switch (self.beautifyBar.iSelectedIndex) {
            
        case 0:
        {
            switch (slider.tag) {
                    
                case 100:
                {
                    self.fSmoothStrength = slider.value;
                    [self setBeautifyValue:self.fSmoothStrength forBeautifyType:BEAUTIFY_SMOOTH_STRENGTH];
                    
                    self.lbl100.text = [NSString stringWithFormat:@"%d" , (int)(self.fSmoothStrength * 100)];
                }
                    break;
                    
                case 101:
                {
                    self.fWhitenStrength = slider.value;
                    [self setBeautifyValue:self.fWhitenStrength forBeautifyType:BEAUTIFY_WHITEN_STRENGTH];
                    self.lbl101.text = [NSString stringWithFormat:@"%d" , (int)(self.fWhitenStrength * 100)];
                }
                    break;
                    
                case 102:
                {
                    self.fReddenStrength = slider.value;
                    [self setBeautifyValue:self.fReddenStrength forBeautifyType:BEAUTIFY_REDDEN_STRENGTH];
                    self.lbl102.text = [NSString stringWithFormat:@"%d" , (int)(self.fReddenStrength * 100)];
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        case 1:
        {
            switch (slider.tag) {
                    
                case 100:
                {
                    self.fShrinkFaceRatio = slider.value;
                    [self setBeautifyValue:self.fShrinkFaceRatio forBeautifyType:BEAUTIFY_SHRINK_FACE_RATIO];
                    self.lbl100.text = [NSString stringWithFormat:@"%d" , (int)(self.fShrinkFaceRatio * 100)];
                }
                    break;
                    
                case 101:
                {
//                    self.fEnlargeEyeRatio = slider.value;
//                    [self setBeautifyValue:self.fEnlargeEyeRatio forBeautifyType:BEAUTIFY_ENLARGE_EYE_RATIO];
//                   self.wbFaceRender.eyeScale = slider.value;
//                   self.lbl101.text = [NSString stringWithFormat:@"%f" ,self.wbFaceRender.eyeScale ];
//                   NSLog(@"eyeScale:%f", self.wbFaceRender.eyeScale);
                }
                    break;
                    
                case 102:
                {
                    self.fShrinkJawRatio = slider.value;
                    [self setBeautifyValue:self.fShrinkJawRatio forBeautifyType:BEAUTIFY_SHRINK_JAW_RATIO];
                    self.lbl102.text = [NSString stringWithFormat:@"%d" , (int)(self.fShrinkJawRatio * 100)];
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        default:
            break;
    }
}

- (void)setBeautifyValue:(float)fValue forBeautifyType:(BeautifyType)iBeautifyType
{
    if (self.render && self.stCamera.bufferQueue) {
        
        __weak typeof(self) weakSelf = self;
        
        dispatch_async(self.stCamera.bufferQueue, ^{
            
            [weakSelf.render setBeautifyValue:fValue forBeautifyType:iBeautifyType];
        });
    }
}


- (void)showDetailWithBeautifyBarItemIndex:(int)iIndex
{
    self.beautifyBGView.hidden = YES;
    
    switch (iIndex) {
            
            
        case 0:
        {
            self.beautifyBGView.hidden = NO;
            [self showBeautifyDetailWithBarItemIndex:iIndex];
        }
            break;
            
        case 1:
        {
            self.beautifyBGView.hidden = NO;
            [self showBeautifyDetailWithBarItemIndex:iIndex];
        }
            break;
            
        default:
            break;
    }
}

- (void)showMenuWithBottomItemIndex:(int)iIndex
{
    
    self.beautifyBGView.hidden = YES;
    
    BOOL isShowMenu = NO;
    
    switch (iIndex) {
            
        case 0:
        {
            isShowMenu = self.effectMenu.isHidden;
            self.effectMenu.hidden = !isShowMenu;
            
            if (isShowMenu) {
                
                [self.effectsList reloadData];
            }
            
            self.beautifyMenu.hidden = YES;
        }
            break;
            
        case 1:
        {
            isShowMenu = self.beautifyMenu.isHidden;
            self.beautifyMenu.hidden = !isShowMenu;
            
            self.effectMenu.hidden = YES;
            
            if (!self.beautifyMenu.hidden) {
                
                switch (self.beautifyBar.iSelectedIndex) {
                    case 0:
                    {
                        self.beautifyBGView.hidden = NO;
                        [self showBeautifyDetailWithBarItemIndex:iIndex];
                    }
                        break;
                    case 1:
                    {
                        self.beautifyBGView.hidden = NO;
                        [self showBeautifyDetailWithBarItemIndex:iIndex];
                    }
                        break;
                        
                    default:
                        break;
                }
            }
        }
            break;
            
        default:
            break;
    }
}

- (void)transitionWithView:(UIView *)view animations:(void (^)(void))animations
{
    [UIView transitionWithView:view
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:animations
                    completion:nil];
}

- (void)showBeautifyDetailWithBarItemIndex:(int)iIndex
{
    if (0 == self.beautifyBar.iSelectedIndex) {
        
        [self transitionWithView:self.beautifyThumb1 animations:^{
            
            self.beautifyThumb1.image = [UIImage imageNamed:@"磨皮"];
        }];
        
        [self transitionWithView:self.beautifyThumb2 animations:^{
            
            self.beautifyThumb2.image = [UIImage imageNamed:@"美白"];
        }];
        
        [self transitionWithView:self.beautifyThumb3 animations:^{
            
            self.beautifyThumb3.image = [UIImage imageNamed:@"刷子"];
        }];
        
        [self transitionWithView:self.lblBeautfiyDesc1 animations:^{
            
            self.lblBeautfiyDesc1.text = @"磨皮";
        }];
        
        [self transitionWithView:self.lblBeautfiyDesc2 animations:^{
            
            self.lblBeautfiyDesc2.text = @"美白";
        }];
        
        [self transitionWithView:self.lblBeautfiyDesc3 animations:^{
            
            self.lblBeautfiyDesc3.text = @"红润";
        }];
        
        self.lbl100.text = [NSString stringWithFormat:@"%d" , (int)(self.fSmoothStrength * 100)];
        self.lbl101.text = [NSString stringWithFormat:@"%d" , (int)(self.fWhitenStrength * 100)];
        self.lbl102.text = [NSString stringWithFormat:@"%d" , (int)(self.fReddenStrength * 100)];
        
        [UIView animateWithDuration:0.3 animations:^{
            
            [self.beautifySlider1 setValue:self.fSmoothStrength animated:YES];
            [self.beautifySlider2 setValue:self.fWhitenStrength animated:YES];
            [self.beautifySlider3 setValue:self.fReddenStrength animated:YES];
            
        }];
    }
    
    if (1 == self.beautifyBar.iSelectedIndex) {
        
        [self transitionWithView:self.beautifyThumb1 animations:^{
            
            self.beautifyThumb1.image = [UIImage imageNamed:@"瘦脸icon"];
        }];
        
        [self transitionWithView:self.beautifyThumb2 animations:^{
            
            self.beautifyThumb2.image = [UIImage imageNamed:@"眼睛icon"];
        }];
        
        [self transitionWithView:self.beautifyThumb3 animations:^{
            
            self.beautifyThumb3.image = [UIImage imageNamed:@"小脸icon"];
        }];
        
        [self transitionWithView:self.lblBeautfiyDesc1 animations:^{
            
            self.lblBeautfiyDesc1.text = @"瘦脸";
        }];
        
        [self transitionWithView:self.lblBeautfiyDesc2 animations:^{
            
            self.lblBeautfiyDesc2.text = @"大眼";
        }];
        
        [self transitionWithView:self.lblBeautfiyDesc3 animations:^{
            
            self.lblBeautfiyDesc3.text = @"小脸";
        }];
        
        self.lbl100.text = [NSString stringWithFormat:@"%d" , (int)(self.fShrinkFaceRatio * 100)];
        self.lbl101.text = [NSString stringWithFormat:@"%d" , (int)(self.fEnlargeEyeRatio * 100)];
        self.lbl102.text = [NSString stringWithFormat:@"%d" , (int)(self.fShrinkJawRatio * 100)];
        
        [UIView animateWithDuration:0.3 animations:^{
            
            [self.beautifySlider1 setValue:self.fShrinkFaceRatio animated:YES];
            [self.beautifySlider2 setValue:self.fEnlargeEyeRatio animated:YES];
            [self.beautifySlider3 setValue:self.fShrinkJawRatio animated:YES];
        }];
    }
}

- (void)cacheThumbnailOfModel:(EffectsCollectionViewCellModel *)model
{
    
    NSString *strFileID = model.material.strMaterialFileID;
    
    id cacheObj = [self.thumbnailCache objectForKey:strFileID];
    
    if (!cacheObj || ![cacheObj isKindOfClass:[UIImage class]]) {
        
        NSString *strThumbnailImagePath = [self.strThumbnailPath stringByAppendingPathComponent:strFileID];
        
        if (![self.fManager fileExistsAtPath:strThumbnailImagePath]) {
            
            [self.thumbnailCache setObject:strFileID forKey:strFileID];
            
            __weak typeof(self) weakSelf = self;
            
            [weakSelf.imageLoadQueue addOperationWithBlock:^{
                
                UIImage *imageDownloaded = nil;
                
                if ([model.material.strThumbnailURL isKindOfClass:[NSString class]]) {
                    
                    NSError *error = nil;
                    
                    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:model.material.strThumbnailURL] options:NSDataReadingMappedIfSafe error:&error];
                    
                    
                    imageDownloaded = [UIImage imageWithData:imageData];
                    
                    if (imageDownloaded) {
                        
                        if ([weakSelf.fManager createFileAtPath:strThumbnailImagePath contents:imageData attributes:nil]) {
                            
                            [weakSelf.thumbnailCache setObject:imageDownloaded forKey:strFileID];
                        }else{
                            
                            [weakSelf.thumbnailCache removeObjectForKey:strFileID];
                        }
                    }else{
                        
                        [weakSelf.thumbnailCache removeObjectForKey:strFileID];
                    }
                }else{
                    
                    [weakSelf.thumbnailCache removeObjectForKey:strFileID];
                }
                
                model.imageThumb = imageDownloaded;
                
                if (SPECIAL_EFFECT == model.material.iMaterialType) {
                    
                    NSArray *arrModels = [weakSelf.effectsDataSource objectForKey:@(weakSelf.effectBar.iSelectedIndex)];
                    
                    if (arrModels.count > model.indexOfItem) {
                        
                        EffectsCollectionViewCellModel *targetModel = arrModels[model.indexOfItem];
                        
                        if ([model.material.strID isEqualToString:targetModel.material.strID]) {
                            
                            EffectsCollectionViewCell *targetCell = (EffectsCollectionViewCell *)[weakSelf.effectsList cellForItemAtIndexPath:[NSIndexPath indexPathForItem:model.indexOfItem inSection:0]];
                            
                            targetCell.model = targetModel;
                        }
                    }
                }
                

            }];
        }else{
            
            UIImage *image = [UIImage imageWithContentsOfFile:strThumbnailImagePath];
            
            if (image) {
                
                [self.thumbnailCache setObject:image forKey:strFileID];
                
            }else{
                
                [self.fManager removeItemAtPath:strThumbnailImagePath error:nil];
            }
        }
    }
}

- (IBAction)onBtnRecord:(UIButton *)btn {
    
    if (!self.isRecording) {
        
        //更新状态是使用synchronized来保证线程同步
        @synchronized(self){
            
            if (_recordStatus != STWriterRecordingStatusIdle) {
                
                @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:@"Start recording" userInfo:nil];
                return;
            }
            
            [self newRecordStatus:STWriterRecordingStatusStartingRecording];
        }
        
         self.isRecording = YES;
        
        _callBackQueue = dispatch_queue_create("com.sensetime.recordercallBack", DISPATCH_QUEUE_SERIAL);
        
        STMovieRecorder *recorder = [[STMovieRecorder alloc] initWithURL:_recorderURL
                                                                delegate:self
                                                           callbackQueue:_callBackQueue];
        
        [recorder addVideoTrackWithSourceFormatDescription:self.outputVideoFormatDescription
                                                 transform:CGAffineTransformIdentity
                                                  settings:self.stCamera.videoCompressingSettings];
        
        [recorder addAudioTrackWithSourceFormatDescription:self.outputAudioFormatDescription
                                                  settings:self.stCamera.audioCompressingSettings];
        
        _stRecorder =  recorder;
        
        [recorder prepareToRecord];
        
        self.recordStartTime = CFAbsoluteTimeGetCurrent();
        
        CGPoint center = btn.center;
        
        CGRect cancelFrame = self.btnCancel.frame;
        self.btnCancel.center = center;
        CGRect finishFrame = self.btnSave.frame;
        self.btnSave.center = center;
        
        [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.btnCancel.frame = cancelFrame;
            self.btnSave.frame = finishFrame;
        } completion:nil];
        
        [self addCycleAnimationWithBtn:btn];
        
        [self.smallvideo recordStart];
        
    }else{
        
        self.isRecording = NO;
        
        if (_recordStatus != STWriterRecordingStatusRecording) {
            
            return;
        }
        
        [self newRecordStatus:STWriterRecordingStatusStoppingRecording];
        
        [_stRecorder finishRecording];
        
        [self removeCycleAnimation];
        
        [self.smallvideo recordStop];
    }
}

- (void)addCycleAnimationWithBtn:(UIButton *)btn
{
    
    _shapeLayer = [CAShapeLayer layer];
    _shapeLayer.strokeColor = [UIColor greenColor].CGColor;
    _shapeLayer.lineWidth = 5;
    _shapeLayer.fillColor = [UIColor clearColor].CGColor;
    [self.view.layer addSublayer:_shapeLayer];
    
    UIBezierPath * path = [UIBezierPath bezierPathWithArcCenter:btn.center
                                                         radius:30
                                                     startAngle:-M_PI_2
                                                       endAngle:M_PI + M_PI_2
                                                      clockwise:YES];
    
    _shapeLayer.path = path.CGPath;

    CABasicAnimation * animation = [CABasicAnimation animation];
    animation.keyPath = @"strokeEnd";
    animation.duration = 20;
    animation.fromValue = @0;

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [_shapeLayer addAnimation:animation forKey:nil];
    });
}
- (void)removeCycleAnimation
{
    [_shapeLayer removeFromSuperlayer];
}
- (IBAction)onBtnCancel:(id)sender {
    
    [self capturing];
}

- (void)capturing
{
    self.btnCamera.hidden = NO;
    self.btnRecord.hidden = NO;
    self.btnSaveSecceed.hidden = YES;
    self.bottomBar.hidden = NO;
    self.btnSave.hidden = YES;
    self.btnCancel.hidden = YES;
}
- (void)onBtnNull
{
    [self listDidSelectWithModel:nil];
}

- (void)listDidSelectWithModel:(EffectsCollectionViewCellModel *)model
{
    self.prepareMaterial = model.material;
    
    if (model) {
        
        SenseArMaterial *material = model.material;
        
        
            if (isDownloaded == model.downloadStatus) {
                
                [self startShowingModel:model];
            }
           
            if (CPT_AD == material.iMaterialType) {
                
                AdsCollectionViewCellModel *adModel = (AdsCollectionViewCellModel *)model;
                
                if (isDownloaded == adModel.downloadStatus) {
                    
                    [self startShowingModel:adModel];
                }
            }
       
    }else{
        
        [self refreshPreCellStatusWithModel:model];
        [self endShowingMaterial];
    }
}

- (void)endShowingMaterial
{
    self.currentMaterial = nil;
    [self.render setMaterial:NULL];
}

- (void)startShowingModel:(EffectsCollectionViewCellModel *)model
{
    
    [self closeMenu];
    
    SenseArRenderStatus iStatus = RENDER_SUCCESS;
//        WBFaceMaterial* faceMaterial = [[WBFaceMaterial alloc] init];
//        faceMaterial.strID = model.material.strID;
//        faceMaterial.strName = model.material.strName;
//        faceMaterial.strMeterialURL = model.material.strMeterialURL;
//        faceMaterial.strThumbnailURL = model.material.strThumbnailURL;
//        faceMaterial.iMaterialType = model.material.iMaterialType;
//        [_wbFaceRender setMaterial:faceMaterial];

     // iStatus =  [self.render setMaterial:model.material];
    

    if (RENDER_SUCCESS == iStatus) {
        
        [self refreshPreCellStatusWithModel:model];
        
        self.currentMaterial = model.material;
        self.dLastActionTipPTS = CFAbsoluteTimeGetCurrent();
        
        if (SPECIAL_EFFECT == model.material.iMaterialType) {
            
            EffectsCollectionViewCell *effctsCell = (EffectsCollectionViewCell *)[self.effectsList cellForItemAtIndexPath:[NSIndexPath indexPathForItem:model.indexOfItem inSection:0]];
            
            effctsCell.isCustomSelected = YES;
        }
        
    }else{
        
        if (RENDER_UNSUPPORTED_MATERIAL == iStatus) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"当前版本不支持该素材" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [alert show];
            });
        }
    }
}
- (BOOL)judgeLibraryAuthorization
{
    ALAuthorizationStatus author = [ALAssetsLibrary authorizationStatus];
    
    if (author == ALAuthorizationStatusRestricted || author == ALAuthorizationStatusDenied)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"请打开相册权限" delegate:nil cancelButtonTitle:@"好的" otherButtonTitles:nil, nil];
        
        [alert show];
        
        return NO;
    }
    
    return YES;
}
- (IBAction)onBtnSave:(id)sender {
    
    self.btnCancel.hidden = YES;
    self.btnSave.hidden = YES;
    
    __block UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithFrame:self.btnRecord.frame];
    
    [self.view addSubview:activity];
    
    [activity startAnimating];
    
    __weak typeof(self) wself = self;
    
    if ([self judgeLibraryAuthorization]) {
        
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        
        [library writeVideoAtPathToSavedPhotosAlbum:_recorderURL
                                    completionBlock:^(NSURL *assetURL, NSError *error){
            
            [[NSFileManager defaultManager] removeItemAtURL:_recorderURL error:NULL];
            
            wself.btnSaveSecceed.hidden = NO;
                                        
            [activity stopAnimating];
                                        
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self onBtnSaveSucceed:nil];
            });
        }];
    }else{
     
        [[NSFileManager defaultManager] removeItemAtURL:_recorderURL error:NULL];
        
        wself.btnSaveSecceed.hidden = NO;
        
        [activity stopAnimating];
    }
}
- (IBAction)onBtnSaveSucceed:(id)sender {
    
    [self capturing];
}

- (void)setIsRecording:(BOOL)isRecording
{
    _isRecording = isRecording;
    
    if (isRecording) {
        
        self.btnCamera.hidden = YES;
        self.bottomBar.hidden = YES;
        self.btnCancel.hidden = YES;
        self.btnSaveSecceed.hidden = YES;
        self.effectMenu.hidden = YES;
        self.beautifyMenu.hidden = YES;
    }else{
        
        self.btnSave.hidden = NO;
        self.btnCancel.hidden = NO;
        self.btnRecord.hidden = YES;
    }
}


#pragma mark - STMovieRecorder delegate
- (void)movieRecorder:(STMovieRecorder *)recorder didFailWithError:(NSError *)error
{
    @synchronized(self){
        
        _stRecorder = nil;
        
        [self newRecordStatus:STWriterRecordingStatusIdle];
    }
}

- (void)movieRecorderDidFinishPreparing:(STMovieRecorder *)recorder
{
    @synchronized( self )
    {
        if ( _recordStatus != STWriterRecordingStatusStartingRecording ) {
            @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:@"Expected to be in StartingRecording state" userInfo:nil];
            return;
        }
        
        [self newRecordStatus:STWriterRecordingStatusRecording];
    }
}

- (void)movieRecorderDidFinishRecording:(STMovieRecorder *)recorder
{
    @synchronized( self )
    {
        if ( _recordStatus != STWriterRecordingStatusStoppingRecording ) {
            @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:@"Expected to be in StoppingRecording state" userInfo:nil];
            return;
        }
        
        [self newRecordStatus: STWriterRecordingStatusIdle];
    }
    
    _stRecorder = nil;
}


- (void)newRecordStatus:(STWriterRecordingStatus)newStatus
{
    _recordStatus = newStatus;
}

- (NSString *)stringForRecordingStatus:(STWriterRecordingStatus)status
{
    NSString *statusString = nil;
    
    switch (status) {
        case STWriterRecordingStatusIdle:
            statusString = @"Idle";
            break;
        case STWriterRecordingStatusStartingRecording:
            statusString = @"StartingRecording";
            break;
        case STWriterRecordingStatusRecording:
            statusString = @"Recording";
            break;
        case STWriterRecordingStatusStoppingRecording:
            statusString = @"stopRecording";
            break;
        default:
            statusString = @"Unknown";
            break;
    }
    
    return statusString;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setExclusiveTouchForButtons:self.view];
}
-(void)setExclusiveTouchForButtons:(UIView *)myView
{
    for (UIView * v in [myView subviews]) {
        if([v isKindOfClass:[UIButton class]])
            [((UIButton *)v) setExclusiveTouch:YES];
        else if ([v isKindOfClass:[UIView class]]){
            [self setExclusiveTouchForButtons:v];
        }
    }
}
- (void)dealloc
{
    
    [self.thumbnailCache removeAllObjects];
    self.thumbnailCache = nil;
    
    [self.stickerListMap removeAllObjects];
    self.stickerListMap = nil;
    
    self.thumbDownlaodQueue = NULL;
    self.imageLoadQueue = nil;
    self.callBackQueue = NULL;
}

#pragma - mark -
#pragma - mark Check License

- (NSString *)getSHA1StringWithData:(NSData *)data
{
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, (unsigned int)data.length, digest);
    
    NSMutableString *strSHA1 = [NSMutableString string];
    
    for (int i = 0 ; i < CC_SHA1_DIGEST_LENGTH ; i ++) {
        
        [strSHA1 appendFormat:@"%02x" , digest[i]];
    }
    
    return strSHA1;
}

- (BOOL)checkActiveCode
{
    NSString *strLicensePath = [[NSBundle mainBundle] pathForResource:@"SENSEME" ofType:@"lic"];
    NSData *dataLicense = [NSData dataWithContentsOfFile:strLicensePath];
    
    NSString *strKeySHA1 = @"SENSEME";
    NSString *strKeyActiveCode = @"ACTIVE_CODE";
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *strStoredSHA1 = [userDefaults objectForKey:strKeySHA1];
    NSString *strLicenseSHA1 = [self getSHA1StringWithData:dataLicense];
    
    NSString *strActiveCode = nil;
    
    NSError *error = nil;
    BOOL bSuccess = NO;
    
    if (strStoredSHA1.length > 0 && [strLicenseSHA1 isEqualToString:strStoredSHA1]) {
        
        // Get current active code
        // In this app active code was stored in NSUserDefaults
        // It also can be stored in other places
        strActiveCode = [userDefaults objectForKey:strKeyActiveCode];
        
        // Check if current active code is available
#if CHECK_LICENSE_WITH_PATH
        
        // use file
        bSuccess = [SenseArMaterialService checkActiveCode:strActiveCode licensePath:strLicensePath error:&error];
#else
        
        // use buffer
        NSData *licenseData = [NSData dataWithContentsOfFile:strLicensePath];
        
        bSuccess = [SenseArMaterialService checkActiveCode:strActiveCode
                                               licenseData:licenseData
                                                     error:&error];
        
#endif
        
        if (bSuccess && !error) {
            
            // check success
            return YES;
        }
    }
    
    /*
     1. check fail
     2. new one
     3. update
     */
    
    
    // generate one
#if CHECK_LICENSE_WITH_PATH
    
    // use file
    strActiveCode = [SenseArMaterialService generateActiveCodeWithLicensePath:strLicensePath
                                                                        error:&error];
    
#else
    
    // use buffer
    strActiveCode = [SenseArMaterialService generateActiveCodeWithLicenseData:dataLicense
                                                                        error:&error];
#endif
    
    if (!strActiveCode.length && error) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"错误提示" message:@"使用 license 文件生成激活码时失败，可能是授权文件过期。" delegate:nil cancelButtonTitle:@"好的" otherButtonTitles:nil, nil];
        
        [alert show];
        
        return NO;
        
    } else {
        
        // Store active code
        
        [userDefaults setObject:strActiveCode forKey:strKeyActiveCode];
        [userDefaults setObject:strLicenseSHA1 forKey:strKeySHA1];
        
        [userDefaults synchronize];
    }
    
    return YES;
}

- (void)setupSenseArServiceAndSmallVideo
{
    // 初始化服务
    self.service = [SenseArMaterialService sharedInstance];
    // 使用AppID , AppKey 进行授权 , 如果不授权将无法使用 SenseArMaterialService 相关接口 .
    
    __weak typeof(self) weakSelf = self;
    [self.service authorizeWithAppID:@"50de50b34c324a6e9a27951715af83d7"
                              appKey:@"1bd49cdfbfca4799b4fc149d098f6530"
                           onSuccess:^{
                               
                               weakSelf.smallvideo = [[SenseArShortVideoClient alloc] init];
                               // 根据实际情况设置主播的属性
                               weakSelf.smallvideo.strID = @"weibo";
                               weakSelf.smallvideo.strName = @"name_weibo";
                               weakSelf.smallvideo.strBirthday = @"19901023";
                               weakSelf.smallvideo.strGender = @"男";
                               weakSelf.smallvideo.strAddress = @"北京市/海淀区";
                               weakSelf.smallvideo.latitude = 39.977813;
                               weakSelf.smallvideo.longitude = 116.317188;
                               weakSelf.smallvideo.iFollowCount = 2000;
                               weakSelf.smallvideo.iFansCount = 2000;
                               weakSelf.smallvideo.strTags = @"游戏";
                               weakSelf.smallvideo.strEmail = @"broadcasteriOS@126.com";
                               
                               SenseArConfigStatus iStatus = [weakSelf.service configureClientWithType:SmallVideo client:weakSelf.smallvideo];
                               
                               if (iStatus == CONFIG_OK) {
                                   
                                   // 设置缓存大小 , 默认为 100M
                                   [weakSelf.service setMaxCacheSize:120000000];
                                   
                                   [weakSelf fetchList];
                                   
                               }else{
                                   
                                   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"服务配置失败" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
                                   
                                   [alert show];
                               }
                               
                               
                           } onFailure:^(SenseArAuthorizeError iErrorCode) {
                               
                               UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"服务初始化失败" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
                               
                               [alert show];
                           }];
    
    
    
}
- (void)setupMaterialRender
{
    // 记录调用 SDK 之前的渲染环境以便在调用 SDK 之后设置回来.
    EAGLContext *preContext = [self getPreContext];
    
    // 调用 SDK 之前需要切换到 SDK 的渲染环境
    [self setCurrentContext:self.glRenderContext];
    
    CVReturn err = CVOpenGLESTextureCacheCreate(kCFAllocatorDefault, NULL, self.glRenderContext, NULL, &_cvTextureCache);
    
    if (err) {
        
        NSLog(@"CVOpenGLESTextureCacheCreate %d" , err);
    }
    
    
    // 创建美颜和贴纸的结果纹理
    [self setupTextureWithPixelBuffer:&_cvBeautifyBuffer
                                    w:_iImageWidth
                                    h:_iImageHeight
                            glTexture:&_textureBeautifyOut
                            cvTexture:&_cvTextureBeautify];
    
    [self setupTextureWithPixelBuffer:&_cvBeautifyShapeBuffer
                                    w:_iImageWidth
                                    h:_iImageHeight
                            glTexture:&_textureBeautifyShapeOut
                            cvTexture:&_cvTextureBeautifySharp];
    
    [self setupTextureWithPixelBuffer:&_cvFilterBuffer
                                    w:_iImageWidth
                                    h:_iImageHeight
                            glTexture:&_textureFilterOut
                            cvTexture:&_cvTextureFilter];

    [self setupTextureWithPixelBuffer:&_cvStickerBuffer
                                    w:_iImageWidth
                                    h:_iImageHeight
                            glTexture:&_textureStickerOut
                            cvTexture:&_cvTextureSticker];
    
    // 获取模型路径
    NSString *strModelPath = [[NSBundle mainBundle] pathForResource:@"sensear_1.1.0"
                                                             ofType:@"model"];
    
    // 根据实际需求决定是否开启美颜和动作检测
    self.render = [SenseArMaterialRender instanceWithModelPath:strModelPath
                                                        config:SENSEAR_ENABLE_HUMAN_ACTION |SENSEAR_ENABLE_BEAUTIFY |SENSEAR_EXPRESSION_DETECT_FACE
                                                       context:self.glRenderContext];
    
    if (self.render) {
        
        // 初始化渲染模块使用的 OpenGL 资源
        [self.render initGLResource];
        
        // 根据需求设置美颜参数
        if (![self.render setBeautifyValue:0.36 forBeautifyType:BEAUTIFY_REDDEN_STRENGTH]) {
            
            STLog(@"set BEAUTIFY_CONTRAST_STRENGTH failed");
        }
        if (![self.render setBeautifyValue:0.74 forBeautifyType:BEAUTIFY_SMOOTH_STRENGTH]) {
            
            STLog(@"set BEAUTIFY_SMOOTH_STRENGTH failed");
        }
        if (![self.render setBeautifyValue:0.02 forBeautifyType:BEAUTIFY_WHITEN_STRENGTH]) {
            
            STLog(@"set BEAUTIFY_WHITEN_STRENGTH failed");
        }
//        if (![self.render setBeautifyValue:0.11 forBeautifyType:BEAUTIFY_SHRINK_FACE_RATIO]) {
//
//            STLog(@"set BEAUTIFY_SHRINK_FACE_RATIO failed");
//        }
//        if (![self.render setBeautifyValue:0.13 forBeautifyType:BEAUTIFY_ENLARGE_EYE_RATIO]) {
//
//            STLog(@"set BEAUTIFY_ENLARGE_EYE_RATIO failed");
//        }
//        if (![self.render setBeautifyValue:0.10 forBeautifyType:BEAUTIFY_SHRINK_JAW_RATIO]) {
//
//            STLog(@"set BEAUTIFY_SHRINK_JAW_RATIO failed");
//        }
        
        
        //render callback
        self.render.renderBegin = ^(NSString *materialID){
            NSLog(@"%@ begin render", materialID);
        };
        self.render.renderEnd = ^(NSString *materialID){
            NSLog(@"%@ end render", materialID);
        };
        
        [self.render forceDetectWithTypes:SENSEAR_FACE_DETECT|SENSEAR_EYE_BLINK|SENSEAR_MOUTH_AH];
        
    }else{
        
        STLog(@"setupMaterialRender failed.");
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"Render初始化失败" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
            
        [alert show];
    }

    
    NSArray*paths =NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString*documentsDirectory =[paths objectAtIndex:0];
    
//    self.wbFaceRender = [[WBFaceRender alloc] initWithContext:self.glRenderContext baseDir:documentsDirectory];
//    self.wbFaceRender.eyeScale = 0.15f;
    // 需要设为之前的渲染环境防止与其他需要 GPU 资源的模块冲突.
    [self setCurrentContext:preContext];
}

-(NSString*) copyFile2Documents:(NSString*)fileName
{
    NSFileManager*fileManager =[NSFileManager defaultManager];
    NSError*error;
    NSArray*paths =NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString*documentsDirectory =[paths objectAtIndex:0];
    
    NSString*destPath =[documentsDirectory stringByAppendingPathComponent:fileName];
    
    //  如果目标目录也就是(Documents)目录没有数据库文件的时候，才会复制一份，否则不复制
    if(![fileManager fileExistsAtPath:destPath]){
        NSString* sourcePath =[[NSBundle mainBundle] pathForResource:fileName ofType:@""];
        [fileManager copyItemAtPath:sourcePath toPath:destPath error:&error];
    }
    return destPath;
}

- (BOOL)setupTextureWithPixelBuffer:(CVPixelBufferRef *)pixelBufferOut
                                  w:(int)iWidth
                                  h:(int)iHeight
                          glTexture:(GLuint *)glTexture
                          cvTexture:(CVOpenGLESTextureRef *)cvTexture
{
    CFDictionaryRef empty = CFDictionaryCreate(kCFAllocatorDefault,
                                               NULL,
                                               NULL,
                                               0,
                                               &kCFTypeDictionaryKeyCallBacks,
                                               &kCFTypeDictionaryValueCallBacks);
    
    CFMutableDictionaryRef attrs =  CFDictionaryCreateMutable(kCFAllocatorDefault,
                                                             1,
                                                             &kCFTypeDictionaryKeyCallBacks,
                                                             &kCFTypeDictionaryValueCallBacks);
    
    CFDictionarySetValue(attrs, kCVPixelBufferIOSurfacePropertiesKey, empty);
    
    CVReturn cvRet = CVPixelBufferCreate(kCFAllocatorDefault,
                                         iWidth,
                                         iHeight,
                                         kCVPixelFormatType_32BGRA,
                                         attrs,
                                         pixelBufferOut);
    
    if (kCVReturnSuccess != cvRet) {
        
        NSLog(@"CVPixelBufferCreate %d" , cvRet);
    }
    
    cvRet = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                         _cvTextureCache,
                                                         *pixelBufferOut,
                                                         NULL,
                                                         GL_TEXTURE_2D,
                                                         GL_RGBA,
                                                         _iImageWidth,
                                                         _iImageHeight,
                                                         GL_BGRA,
                                                         GL_UNSIGNED_BYTE,
                                                         0,
                                                         cvTexture);
    
    CFRelease(attrs);
    CFRelease(empty);
    
    if (kCVReturnSuccess != cvRet) {
        
        NSLog(@"CVOpenGLESTextureCacheCreateTextureFromImage %d" , cvRet);
        
        return NO;
    }
    
    *glTexture = CVOpenGLESTextureGetName(*cvTexture);
    glBindTexture(CVOpenGLESTextureGetTarget(*cvTexture), *glTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glBindTexture(GL_TEXTURE_2D, 0);
    
    return YES;
}

#pragma - mark -
#pragma - mark Setup UI
- (CGRect)getZoomedRectWithImageWidth:(int)iWidth
                               height:(int)iHeight
                               inRect:(CGRect)rect
                           scaleToFit:(BOOL)bScaleToFit
{   
    float fScaleX = iWidth / CGRectGetWidth(rect);
    float fScaleY = iHeight / CGRectGetHeight(rect);
    float fScale = bScaleToFit ? fmaxf(fScaleX, fScaleY) : fminf(fScaleX, fScaleY);
    
    
    iWidth /= fScale;
    iHeight /= fScale;
    
    CGFloat fX = rect.origin.x - (iWidth - rect.size.width) / 2.0f;
    CGFloat fY = rect.origin.y - (iHeight - rect.size.height) / 2.0f;
    
    CGRect rectRet = CGRectMake(fX, fY, iWidth, iHeight);
    
    return rectRet;
}

- (void)setupCamera
{
    self.stCamera = [[STCamera alloc] init];
    self.stCamera.bOutputYUV = NO;
    self.stCamera.sessionPreset = AVCaptureSessionPreset1280x720;
    self.stCamera.delegate = self;
    self.stCamera.iFPS = 25;
}

- (void)setupPreview
{
    // 创建 OpenGL 上下文 , 根据实际情况与预览使用同一个 context 或 shareGroup .
    self.glRenderContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    
    EAGLContext *previewContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2 sharegroup:self.glRenderContext.sharegroup];
    
    CGRect previewRect = [self getZoomedRectWithImageWidth:_iImageWidth height:_iImageHeight inRect:[UIScreen mainScreen].bounds scaleToFit:NO];
    
    self.glPreview = [[STGLPreview alloc] initWithFrame:previewRect context:previewContext];
    [self.view insertSubview:self.glPreview atIndex:0];
}

#pragma - mark -
#pragma - mark Filter path

- (NSArray *)getFilterModelPaths
{
    NSFileManager *fileManage = [[NSFileManager alloc] init];
    
    NSString *strBundlePath = [[NSBundle mainBundle] resourcePath];
    
    NSArray *arrFileNames = [fileManage contentsOfDirectoryAtPath:strBundlePath error:nil];
    
    NSMutableArray *arrModels = [NSMutableArray array];
    
    for(NSString *strFileName in arrFileNames){
        if ([strFileName hasSuffix:@"model"] && [strFileName hasPrefix:@"filter_style"]) {
            
            [arrModels addObject:[NSString pathWithComponents:@[strBundlePath, strFileName]]];
        }
    }
    
    NSString *strDocumentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    
    arrFileNames = [fileManage contentsOfDirectoryAtPath:strDocumentsPath error:nil];
    
    for (NSString *strFileName in arrFileNames) {
        
        if ([strFileName hasSuffix:@"zip"] && [strFileName hasPrefix:@"filter_style"]) {
            
            [arrModels addObject:[NSString pathWithComponents:@[strDocumentsPath, strFileName]]];
        }
    }
    
    return arrModels;
}

- (EAGLContext *)getPreContext
{
    return [EAGLContext currentContext];
}

- (void)setCurrentContext:(EAGLContext *)context
{
    if ([EAGLContext currentContext] != context) {
        
        [EAGLContext setCurrentContext:context];
    }
}


- (NSString *)getActionDescribtion:(SenseArTriggerAction)iActionType
{
    switch (iActionType) {
            
            // 张嘴
        case MOUTH_AH:
        {
            return @"张嘴";
        }
            break;
            
            // 眨眼
        case EYE_BLINK:
        {
            return @"眨眼";
        }
            break;
            
            // 点头
        case HEAD_PITCH:
        {
            return @"点头";
        }
            break;
            
            // 摇头
        case HEAD_YAW:
        {
            return @"摇头";
        }
            break;
            
            // 挑眉
        case BROW_JUMP:
        {
            return @"挑眉";
        }
            break;
            
            // 手掌
        case HAND_PALM:
        {
            return @"手掌";
        }
            break;
            
            // 大拇哥
        case HAND_GOOD:
        {
            return @"大拇哥";
        }
            break;
            
            // 托手
        case HAND_HOLDUP:
        {
            return @"托手";
        }
            break;
            
            // 爱心手
        case HAND_LOVE:
        {
            return @"爱心手";
        }
            break;
            
            // 恭贺(抱拳)
        case HAND_CONGRATULATE:
        {
            return @"抱拳";
        }
            break;
            
            // 单手比爱心
        case HAND_FINGER_HEART:
        {
            return @"单手爱心";
        }
            break;
            
            // 平行手指
        case HAND_TWO_INDEX_FINGER:
        {
            return @"平行手指";
        }
            break;
            
            // OK手势
        case HAND_OK:
        {
            return @"OK手势";
        }
            break;
            
            // 剪刀手
        case HAND_SCISSOR:
        {
            return @"剪刀手";
        }
            break;
            
            // 手枪
        case HAND_PISTOL:
        {
            return @"手枪";
        }
            break;
            
            // 指尖
        case FINGER_INDEX:
        {
            return @"指尖";
        }
            break;
            
        default:
        {
            return @"未知动作";
        }
            
            break;
    }
}

- (void)endShowingAd
{
    self.currentMaterial = nil;
    [self.render setMaterial:NULL];
}

- (SenseArRotateType)getRotateType
{
    BOOL isFrontCamera = self.stCamera.devicePosition == AVCaptureDevicePositionFront;
    BOOL isVideoMirrored = self.stCamera.videoConnection.isVideoMirrored;
    UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
    
    switch (deviceOrientation) {
            
        case UIDeviceOrientationPortrait:
            return CLOCKWISE_ROTATE_0;
            
        case UIDeviceOrientationPortraitUpsideDown:
            return CLOCKWISE_ROTATE_180;
            
        case UIDeviceOrientationLandscapeLeft:
            return ((isFrontCamera && isVideoMirrored) || (!isFrontCamera && !isVideoMirrored)) ? CLOCKWISE_ROTATE_270 : CLOCKWISE_ROTATE_90;
            
        case UIDeviceOrientationLandscapeRight:
            return ((isFrontCamera && isVideoMirrored) || (!isFrontCamera && !isVideoMirrored)) ? CLOCKWISE_ROTATE_90 : CLOCKWISE_ROTATE_270;
            
        default:
            return CLOCKWISE_ROTATE_0;
    }
}

- (void)reportSmallVideoInformation
{
    
    [self.smallvideo reportWithUseId:@"weibo"
                             videoID:@"weibo"
                           videoLink:@"http://www.sensetime.com"
                          reportData: [self.smallvideo getReportData]
                          completion:^(BOOL isSuccess, NSString *strMessage) {
//                              NSLog(@"%@", strMessage);
                          }];
}


#pragma - mark -
#pragma - mark Touch Event

//- (IBAction)onBtnBack:(id)sender {
//
//    // 需要在 releaseGLResource 之前调用
//    [self endShowingAd];
//
//    [self reportSmallVideoInformation];
//
//    dispatch_async(self.stCamera.bufferQueue, ^{
//
//        EAGLContext *preContext = [self getPreContext];
//
//        [self setCurrentContext:self.glRenderContext];
//
//        // 释放渲染模块占用的 OpenGL 相关资源
//        [self.render releaseGLResource];
//
//        glDeleteTextures(1, &_textureOriginalIn);
//        glDeleteTextures(1, &_textureBeautifyOut);
//        glDeleteTextures(1, &_textureStickerOut);
//        glDeleteTextures(1, &_textureFilterOut);
//
//        CVPixelBufferRelease(_cvTextureBeautify);
//        CVPixelBufferRelease(_cvTextureSticker);
//        CVPixelBufferRelease(_cvTextureFilter);
//
//        CVPixelBufferRelease(_cvBeautifyBuffer);
//        CVPixelBufferRelease(_cvStickerBuffer);
//        CVPixelBufferRelease(_cvFilterBuffer);
//
//        if (self.outputAudioFormatDescription) {
//
//            self.outputAudioFormatDescription = NULL;
//        }
//
//        if (self.outputVideoFormatDescription) {
//
//            self.outputVideoFormatDescription = NULL;
//        }
//
//        if (_cvOriginalTexture) {
//
//            CFRelease(_cvOriginalTexture);
//            _cvOriginalTexture = NULL;
//        }
//
//        if (_cvTextureCache) {
//
//            CFRelease(_cvTextureCache);
//            _cvTextureCache = NULL;
//        }
//
//
//        [self setCurrentContext:preContext];
//    });
//
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
//
//    [self.smallvideo recordStop];
//
//    // 关闭 SenseArMaterialService 释放资源
//    [SenseArMaterialService releaseResources];
//
//    self.service = nil;
//
//    [self.imageLoadQueue cancelAllOperations];
//
//    [self.navigationController popViewControllerAnimated:YES];
//}

- (IBAction)onBtnCamera:(UIButton *)sender {

    sender.selected = !sender.selected;
    
    self.stCamera.devicePosition = sender.selected ? AVCaptureDevicePositionBack : AVCaptureDevicePositionFront;
}
#pragma - mark -
#pragma - mark Private Method

- (void)showActionTipsIfNeed
{
    __weak typeof(self) weakSelf = self;
    
    if (self.currentMaterial) {
        
        if ((CFAbsoluteTimeGetCurrent() - self.dLastActionTipPTS) > kACTION_TIP_STAY_TIME) {
            
            if (![self.lblActionTip isHidden]) {
                
                __weak typeof(self) weakSelf = self;
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [weakSelf.lblActionTip setHidden:YES];
                    [weakSelf.actionTipImageView setImage:nil];
                    [weakSelf.lblActionTip setText:@""];
                });
            }
            
        }else{
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [weakSelf.lblActionTip setHidden:NO];
                
                NSString *strTriggerActionTip = @"";
                
                for (SenseArMaterialAction * materialAction in self.currentMaterial.arrMaterialTriggerActions) {
                    
                    strTriggerActionTip = [strTriggerActionTip stringByAppendingString:materialAction.strTriggerActionTip];
                }
                
                [self.lblActionTip setText:strTriggerActionTip];
                
                [self.actionTipImageView setHidden:NO];
                
                switch (self.currentMaterial.arrMaterialTriggerActions.firstObject.iTriggerAction) {
                        
                        // 张嘴
                    case MOUTH_AH:
                    {
                        [self.actionTipImageView setImage:[UIImage imageNamed:@"张嘴.png"]];
                    }
                        break;
                        
                        // 眨眼
                    case EYE_BLINK:
                    {
                        [self.actionTipImageView setImage:[UIImage imageNamed:@"眨眼.png"]];
                    }
                        break;
                        
                        // 点头
                    case HEAD_PITCH:
                    {
                        [self.actionTipImageView setImage:[UIImage imageNamed:@"点头.png"]];
                    }
                        break;
                        
                        // 摇头
                    case HEAD_YAW:
                    {
                        [self.actionTipImageView setImage:[UIImage imageNamed:@"转头.png"]];
                    }
                        break;
                        
                        // 挑眉
                    case BROW_JUMP:
                    {
                        [self.actionTipImageView setImage:[UIImage imageNamed:@"挑眉.png"]];
                    }
                        break;
                        
                        // 手掌
                    case HAND_PALM:
                    {
                        [self.actionTipImageView setImage:[UIImage imageNamed:@"手掌.png"]];
                    }
                        break;
                        
                        // 大拇哥
                    case HAND_GOOD:
                    {
                        [self.actionTipImageView setImage:[UIImage imageNamed:@"大拇哥.png"]];
                    }
                        break;
                        
                        // 托手
                    case HAND_HOLDUP:
                    {
                        [self.actionTipImageView setImage:[UIImage imageNamed:@"托起.png"]];
                    }
                        break;
                        
                        // 爱心手
                    case HAND_LOVE:
                    {
                        [self.actionTipImageView setImage:[UIImage imageNamed:@"爱心.png"]];
                    }
                        break;
                        
                        // 恭贺(抱拳)
                    case HAND_CONGRATULATE:
                    {
                        [self.actionTipImageView setImage:[UIImage imageNamed:@"抱拳.png"]];
                    }
                        break;
                        
                        // 单手比爱心
                    case HAND_FINGER_HEART:
                    {
                        [self.actionTipImageView setImage:[UIImage imageNamed:@"单手爱心.png"]];
                    }
                        break;
                        
                        // 平行手指
                    case HAND_TWO_INDEX_FINGER:
                    {
                        [self.actionTipImageView setImage:[UIImage imageNamed:@"平行手指.png"]];
                    }
                        break;
                        
                        // OK手势
                    case HAND_OK:
                    {
                        [self.actionTipImageView setImage:[UIImage imageNamed:@"OK.png"]];
                    }
                        break;
                        
                        // 剪刀手
                    case HAND_SCISSOR:
                    {
                        [self.actionTipImageView setImage:[UIImage imageNamed:@"剪刀手.png"]];
                    }
                        break;
                        
                        // 手枪
                    case HAND_PISTOL:
                    {
                        [self.actionTipImageView setImage:[UIImage imageNamed:@"手枪.png"]];
                    }
                        break;
                        
                        // 指尖
                    case FINGER_INDEX:
                    {
                        [self.actionTipImageView setImage:[UIImage imageNamed:@"指尖.png"]];
                    }
                        break;
                        
                        
                    default:
                    {
                        [self.actionTipImageView setImage:nil];
                    }
                        
                        break;
                }
            });
        }
        
    }else{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [weakSelf.lblActionTip setHidden:YES];
            [weakSelf.actionTipImageView setImage:nil];
        });
    }
}

- (void)solvePaddingImage:(Byte *)pImage width:(int)iWidth height:(int)iHeight bytesPerRow:(int *)pBytesPerRow
{
    
    int iBytesPerPixel = *pBytesPerRow / iWidth;
    int iBytesPerRowCopied = iWidth * iBytesPerPixel;
    int iCopiedImageSize = sizeof(Byte) * iWidth * iBytesPerPixel * iHeight;
    
    Byte *pCopiedImage = (Byte *)malloc(iCopiedImageSize);
    memset(pCopiedImage, 0, iCopiedImageSize);
    
    for (int i = 0; i < iHeight; i ++) {
        
        memcpy(pCopiedImage + i * iBytesPerRowCopied,
               pImage + i * *pBytesPerRow,
               iBytesPerRowCopied);
    }
    
    memcpy(pImage, pCopiedImage, iCopiedImageSize);
    free(pCopiedImage);
    
    *pBytesPerRow = iBytesPerRowCopied;
}





- (BOOL)isMaterialTriggered:(NSString *)strMaterialID frameActionInfo:(SenseArFrameActionInfo *)frameActionInfo
{
    STMaterialDisplayConfig *config = [self.dicMaterialDisplayConfig objectForKey:strMaterialID];
    
    if (!config) {
        
        return NO;
    }
    
    if (!frameActionInfo) {
        
        return NO;
    }
    
    for (SenseArFace *arFace in frameActionInfo.arrFaces) {
        
        if ((arFace.iAction & config.iTriggerType) > 0) {
            
            return YES;
        }
    }
    
    for (SenseArHand *arHand in frameActionInfo.arrHands) {
        
        if ((arHand.iAction & config.iTriggerType) > 0) {
            
            return YES;
        }
    }
    
    return NO;
}

- (void)changeToNextPartsWithMaterialID:(NSString *)strMaterialID
                          materialParts:(NSArray <SenseArMaterialPart *>*)arrMaterialParts
{
    STMaterialDisplayConfig *config = [self.dicMaterialDisplayConfig objectForKey:strMaterialID];
    
    NSArray <NSString *> *arrNextParts = [config nextParts];
    
    if (arrNextParts.count) {
        
        for (SenseArMaterialPart *materialPart in arrMaterialParts) {
            
            for (NSString *strPartName in arrNextParts) {
                
                if ([materialPart.strPartName isEqualToString:strPartName]) {
                    
                    materialPart.isEnable = YES;
                    
                    break;
                }else{
                    
                    materialPart.isEnable = NO;
                }
            }
        }
        
        [self.render enableMaterialParts:arrMaterialParts];
    }
}

- (void)resetCurrentPartsIndexWithID:(NSString *)strID
{
    STMaterialDisplayConfig *config = [self.dicMaterialDisplayConfig objectForKey:strID];
    
    config.iCurrentPartsIndex = -1;
}





- (void)getTexturezFromOriginalInput:(CVPixelBufferRef)pixelBuffer
{
    // 原图纹理
    CVReturn cvRet = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                                  _cvTextureCache,
                                                                  pixelBuffer,
                                                                  NULL,
                                                                  GL_TEXTURE_2D,
                                                                  GL_RGBA,
                                                                  _iImageWidth,
                                                                  _iImageHeight,
                                                                  GL_BGRA,
                                                                  GL_UNSIGNED_BYTE,
                                                                  0,
                                                                  &_cvOriginalTexture);
    
    if (!_cvOriginalTexture || kCVReturnSuccess != cvRet) {
        
        NSLog(@"CVOpenGLESTextureCacheCreateTextureFromImage %d" , cvRet);
        
    }
    
    if (_cvOriginalTexture) {
        
        _textureOriginalIn = CVOpenGLESTextureGetName(_cvOriginalTexture);
        glBindTexture(GL_TEXTURE_2D , _textureOriginalIn);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glBindTexture(GL_TEXTURE_2D, 0);
    }else{
        
        NSLog(@"_cvOriginalTexture is null");
    }
}

#pragma - mark -
#pragma - mark STCameraDelegate

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    // 应用未激活状态不做任何渲染
    if ([UIApplication sharedApplication].applicationState != UIApplicationStateActive) {
        
        return;
    }
    
    if (!self.isAppActive) {
        
        return;
    }
    
    if (!self.cvTextureCache) {
        
        return;
    }
    
    __weak typeof(self) wself = self;
    
    if ( connection == wself.stCamera.audioConnection )
    {
        wself.outputAudioFormatDescription = CMSampleBufferGetFormatDescription( sampleBuffer );
        
        if ( wself.recordStatus == STWriterRecordingStatusRecording ) {
            
            [wself.stRecorder appendAudioSampleBuffer:sampleBuffer];
        }
    }
    
    // get pts
    CMTime timestamp = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
    
    
    if (wself.isRecording && ((CFAbsoluteTimeGetCurrent() - wself.recordStartTime) > kRECORD_MAX_TIME)) {

        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self onBtnRecord:nil];
        });
    }
    
    // video
    if (connection == wself.stCamera.videoConnection) {
        
        TIMELOG(totalCost)
        
        CVPixelBufferRef pixelBuffer = (CVPixelBufferRef)CMSampleBufferGetImageBuffer(sampleBuffer);
        
        [self pixelBufferToAIBuffer:pixelBuffer];
        
        CVPixelBufferLockBaseAddress(_ForAIInputBuffer, 0);
                
        int iBytesPerRow = (int)CVPixelBufferGetBytesPerRow(_ForAIInputBuffer);
        
        int iWidth = (int)CVPixelBufferGetWidth(_ForAIInputBuffer);
        int iHeight = (int)CVPixelBufferGetHeight(_ForAIInputBuffer);
        
        size_t iTop , iLeft , iBottom , iRight = 0;
        CVPixelBufferGetExtendedPixels(_ForAIInputBuffer, &iLeft, &iRight, &iTop, &iBottom);
        
        iWidth += ((int)iLeft + (int)iRight);
        iHeight += ((int)iTop + (int)iBottom);
        
        iBytesPerRow += (iLeft + iRight);
        
        _iImageWidth = iWidth;
        
        _iImageHeight = iHeight;
        
        // 记录之前的渲染环境
        EAGLContext *preContext = [wself getPreContext];
        // 设置 SDK 的渲染环境
        [wself setCurrentContext:wself.glRenderContext];
        
        [wself getTexturezFromOriginalInput:_ForAIInputBuffer];
        
        GLuint textureResult =_textureOriginalIn;
        
        CVPixelBufferRef resultPixelBufffer = _ForAIInputBuffer;
     
        
        wself.outputVideoFormatDescription = CMSampleBufferGetFormatDescription( sampleBuffer );
        
        if ( wself.recordStatus == STWriterRecordingStatusRecording ) {
            
            [wself.stRecorder appendVideoPixelBuffer:resultPixelBufffer withPresentationTime:timestamp];
        }
        
        
        // 恢复之前的渲染环境
        [wself setCurrentContext:preContext];
        
        [wself.glPreview renderTexture:textureResult];
        
        [self showActionTipsIfNeed];
        
        CVPixelBufferUnlockBaseAddress(_ForAIInputBuffer, 0);
        CVOpenGLESTextureCacheFlush(_cvTextureCache, 0);
        
        if (_cvOriginalTexture) {
            
            CFRelease(_cvOriginalTexture);
            _cvOriginalTexture = NULL;
        }
        
        TIMEPRINT(totalCost, "总耗时")
    }
}
- (void)refreshPreCellStatusWithModel:(EffectsCollectionViewCellModel *)model
{
    if (!model) {
        
        if (self.selectedEffectsModel) {
            
            EffectsCollectionViewCell *effectsCell = (EffectsCollectionViewCell *)[self.effectsList cellForItemAtIndexPath:[NSIndexPath indexPathForItem:self.selectedEffectsModel.indexOfItem inSection:0]];
            
            effectsCell.isCustomSelected = NO;
            
            self.selectedEffectsModel = nil;
        }
    }else{
            
        if (self.selectedEffectsModel.indexOfItem != model.indexOfItem) {
            
            EffectsCollectionViewCell *effectsCell = (EffectsCollectionViewCell *)[self.effectsList cellForItemAtIndexPath:[NSIndexPath indexPathForItem:self.selectedEffectsModel.indexOfItem inSection:0]];
            
            effectsCell.isCustomSelected = NO;
        }
        
        self.selectedEffectsModel = model;
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    
    CGPoint touchPosition = [touches.allObjects.firstObject locationInView:self.view];
    
    
    ////
    
    if (   !self.effectMenu.hidden
        || !self.beautifyMenu.hidden) {
        
        CGRect responseArea = CGRectMake(0, 0, SCREEN_WIDTH, CGRectGetMinY(self.effectMenu.frame));
        
        if (CGRectContainsPoint(responseArea, touchPosition)) {
                [self closeMenu];
        }
    }
    
    ////
}
- (void)closeMenu
{
    self.effectMenu.hidden = YES;
    self.beautifyMenu.hidden = YES;
    
}
#pragma - mark -
#pragma - mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
            
        case 0:
        {
            //[self onBtnBack:nil];
        }
            break;
            
        default:
            break;
    }
}

#pragma - mark -
#pragma - mark EffectsCollectionViewCellDelegate


- (void)btnDownloadDidClickInEffectsCellWithModel:(EffectsCollectionViewCellModel *)model
{
    if (![self.service isMaterialDownloaded:model.material]) {
        
        model.downloadStatus = isDownloading;
        
        EffectsCollectionViewCell *cell = (EffectsCollectionViewCell *)[self.effectsList cellForItemAtIndexPath:[NSIndexPath indexPathForItem:model.indexOfItem inSection:0]];
        
        cell.model = model;
        
        __weak typeof(self) weakSelf = self;
        
        [self.service downloadMaterial:model.material
                             onSuccess:^(SenseArMaterial *material)
         {
             
             if (model) {
                 
                 // 判断素材文件ID是否变更
                 if ([material.strMaterialFileID isEqualToString:model.material.strMaterialFileID]) {
                     
                     model.downloadStatus = isDownloaded;
                 }else{
                     // 不对现在model状态造成影响
                 }
                 
                 NSArray *arrModels = [weakSelf.effectsDataSource objectForKey:@(weakSelf.effectBar.iSelectedIndex)];
                 
                 if (arrModels.count > model.indexOfItem) {
                     
                     EffectsCollectionViewCellModel *targetModel = arrModels[model.indexOfItem];
                     
                     if ([model.material.strID isEqualToString:targetModel.material.strID]) {
                         
                         EffectsCollectionViewCell *targetCell = (EffectsCollectionViewCell *)[weakSelf.effectsList cellForItemAtIndexPath:[NSIndexPath indexPathForItem:model.indexOfItem inSection:0]];
                         
                         targetCell.model = targetModel;
                     }
                 }
             }
         }
                             onFailure:^(SenseArMaterial *material, int iErrorCode, NSString *strMessage) {
                                 
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     
                                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"错误提示"
                                                                                     message:strMessage
                                                                                    delegate:nil
                                                                           cancelButtonTitle:@"好的"
                                                                           otherButtonTitles:nil, nil];
                                     
                                     [alert show];
                                 });
                                 
                                 if (model) {
                                     
                                     // 判断素材文件ID是否变更
                                     if ([material.strMaterialFileID isEqualToString:model.material.strMaterialFileID]
                                         &&  model.downloadStatus) {
                                         
                                         model.downloadStatus = notDownload;
                                         
                                     }else{
                                         // 不对现在model状态造成影响
                                     }
                                     
                                     NSArray *arrModels = [weakSelf.effectsDataSource objectForKey:@(weakSelf.effectBar.iSelectedIndex)];
                                     
                                     if (arrModels.count > model.indexOfItem) {
                                         
                                         EffectsCollectionViewCellModel *targetModel = arrModels[model.indexOfItem];
                                         
                                         if ([model.material.strID isEqualToString:targetModel.material.strID]) {
                                             
                                             EffectsCollectionViewCell *targetCell = (EffectsCollectionViewCell *)[weakSelf.effectsList cellForItemAtIndexPath:[NSIndexPath indexPathForItem:model.indexOfItem inSection:0]];
                                             
                                             targetCell.model = targetModel;
                                         }
                                     }
                                 }
                             }
                            onProgress:nil];
    }else{
        
        model.downloadStatus = isDownloaded;
        
        EffectsCollectionViewCell *cell = (EffectsCollectionViewCell *)[self.effectsList cellForItemAtIndexPath:[NSIndexPath indexPathForItem:model.indexOfItem inSection:0]];
        
        cell.model = model;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - AI 背景分割

-(void)createAIBuffer{
    //input buffer

    NSDictionary *options = @{
                              (NSString*)kCVPixelBufferCGImageCompatibilityKey : @YES,
                              (NSString*)kCVPixelBufferCGBitmapContextCompatibilityKey : @YES,
                              };
    
    
    CVReturn cvRet = CVPixelBufferCreate(kCFAllocatorDefault,
                                         256,
                                         320,
                                         kCVPixelFormatType_32ARGB,
                                         (__bridge CFDictionaryRef) options,
                                         &_ForAIInputBuffer);
    if (kCVReturnSuccess != cvRet) {
        NSLog(@"CVPixelBufferCreate %d" , cvRet);
    }
    
    //output buffer
    
    cvRet = CVPixelBufferCreate(kCFAllocatorDefault,
                                         256,
                                         320,
                                         kCVPixelFormatType_32BGRA,
                                         (__bridge CFDictionaryRef) options,
                                         &_ForAIOutputBuffer);
    if (kCVReturnSuccess != cvRet) {
        NSLog(@"CVPixelBufferCreate %d" , cvRet);
    }
    
    bytes = new double[256 * 320];
}

-(void) pixelBufferToAIBuffer:(CVPixelBufferRef)imageBuffer{
    //第一步:转化为UIImage
    CVPixelBufferLockBaseAddress(imageBuffer, 0);
    void *baseAddress = CVPixelBufferGetBaseAddress(imageBuffer);
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    size_t bufferSize = CVPixelBufferGetDataSize(imageBuffer);
    size_t bytesPerRow = CVPixelBufferGetBytesPerRowOfPlane(imageBuffer, 0);
    
    CGColorSpaceRef rgbColorSpace = CGColorSpaceCreateDeviceRGB();
    CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, baseAddress, bufferSize, NULL);
    
    CGImageRef cgImage = CGImageCreate(width, height, 8, 32, bytesPerRow, rgbColorSpace, kCGImageAlphaNoneSkipFirst, provider, NULL, true, kCGRenderingIntentDefault);
    UIImage *image = [UIImage imageNamed:@"WechatIMG78.jpeg"];
    CGImageRelease(cgImage);
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(rgbColorSpace);
    CVPixelBufferUnlockBaseAddress(imageBuffer, 0);
    //第二步: 转化成320*256的图片
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(256, 320),true,0.0);
    [image drawInRect:CGRectMake(0, 0, 256, 320)];
    UIImage* newImage =UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    //第三步：画到pixelbuffer里面
    CGColorSpaceRef rgbColorSpace2 = CGColorSpaceCreateDeviceRGB();
    CVPixelBufferLockBaseAddress(_ForAIInputBuffer, 0);
    void* pxdata = CVPixelBufferGetBaseAddress(_ForAIInputBuffer);
    CGContextRef context = CGBitmapContextCreate(pxdata, newImage.size.width,
                                                 newImage.size.height,8,CVPixelBufferGetBytesPerRow(_ForAIInputBuffer), rgbColorSpace2,
                                                 kCGImageAlphaNoneSkipFirst);
    CGContextTranslateCTM(context,0,newImage.size.height);
    CGContextScaleCTM(context,1.0,-1.0);
    UIGraphicsPushContext(context);
    [newImage drawInRect:CGRectMake(0, 0,  newImage.size.width,  newImage.size.height)];
    UIGraphicsPopContext();
    CVPixelBufferUnlockBaseAddress(_ForAIInputBuffer, 0);
    //第四步:输入AI模块，得到结果
    bgsegment* model = [[bgsegment alloc] init];
    bgsegmentOutput * output;
    output = [model predictionFromPlaceholder__0:_ForAIInputBuffer error:nil];
    //第五步：转化成UIImage图
    CVPixelBufferRef out_buffer = output.logits_out__0;
    CVPixelBufferLockBaseAddress(out_buffer, 0);
    CGColorSpaceRef GraySpace = CGColorSpaceCreateDeviceGray();
    int bitsPerComponent = 8;
    int otargetWidth = (int)CVPixelBufferGetWidthOfPlane(out_buffer, 0);
    int otargetHeight = (int)CVPixelBufferGetHeightOfPlane(out_buffer, 0);
    int obytesPerRow = (int)CVPixelBufferGetBytesPerRowOfPlane(out_buffer, 0);
    void* obaseAddress = CVPixelBufferGetBaseAddressOfPlane(out_buffer, 0);
    CGContextRef ocontext = CGBitmapContextCreate(obaseAddress,otargetWidth ,
                                                otargetHeight,bitsPerComponent,obytesPerRow, GraySpace,
                                                kCGImageAlphaNone);
    CGContextTranslateCTM(ocontext,0,newImage.size.height);
    CGContextScaleCTM(context,1.0,-1.0);
    CVPixelBufferUnlockBaseAddress(out_buffer, 0);
    CGImageRef oCgImage = CGBitmapContextCreateImage(ocontext);
    UIImage* oImage = [UIImage imageWithCGImage:oCgImage];
    
    return;
}


@end

