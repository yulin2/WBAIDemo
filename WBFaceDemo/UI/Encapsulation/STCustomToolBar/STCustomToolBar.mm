//
//  STCustomToolBar.m
//  SenseArDemo
//
//  Created by sluin on 2017/8/28.
//  Copyright © 2017年 SenseTime. All rights reserved.
//

#import "STCustomToolBar.h"
#import "STUtil.h"

@implementation STCustomToolBarItem

+ (instancetype)itemWithTitle:(NSString *)strTitle
                 defaultImage:(UIImage *)defaultImage
                 selecedImage:(UIImage *)selectedImage
{
    STCustomToolBarItem *item = [[self alloc] init];
    
    item.strTitle = strTitle;
    item.defaultImage = defaultImage;
    item.selecedImage = selectedImage;
    
    return item;
}

STCustomToolBarItem *createCustomToolBarItem(NSString *strTitle ,
                                             UIImage *defaultImage ,
                                             UIImage *selectedImage)
{
    STCustomToolBarItem *item = [[STCustomToolBarItem alloc] init];
    
    item.strTitle = strTitle;
    item.defaultImage = defaultImage;
    item.selecedImage = selectedImage;
    
    return item;
}

@end

@interface STCustomToolBar ()

@property (nonatomic , copy) void(^callback)(int iSelectedIndex);
@property (nonatomic , strong) UIView *selectedMark;
@property (nonatomic , strong) NSArray <STCustomToolBarItem *> *arrItems;

@property (nonatomic , strong) NSMutableDictionary *dicImageViews;
@property (nonatomic , strong) NSMutableDictionary *dicLabels;

@property (nonatomic , strong) UIColor *highlightedColor;

@end

@implementation STCustomToolBar

- (instancetype)initWithFrame:(CGRect)frame
                scrollEnabled:(BOOL)scrollEnabled
                   equalParts:(BOOL)isEqualParts
        firstItemLeadingSpace:(int)iFirstItemLeadingSpace
                      spacing:(int)iSpacing
             highlightedColor:(UIColor *)highlightedColor
                markViewStyle:(STCustomToolBarMarkViewStyle)markViewStyle
                        items:(NSArray<STCustomToolBarItem *> *)arrItems
               selectCallback:(void (^)(int))callback
{
    if (arrItems.count) {
        
        self = [super initWithFrame:frame];
        
        if (!self) {
            
            return nil;
        }
        
        self.iSelectedIndex = 0;
        self.callback = callback;
        self.arrItems = arrItems;
        
        self.backgroundColor = UIColorFromRGB(0x0f131c);
        self.highlightedColor = highlightedColor;
        
        int iLastMaxX = iFirstItemLeadingSpace;
        int iPadding = 2;
        
        int iImageWidth ,iLabelWidth , iLabelHeight = 0;
        
        iLabelHeight = CGRectGetHeight(frame);
        iImageWidth = iLabelHeight - 2 * iPadding;
        
        if (isEqualParts) {
            
            iLabelWidth = CGRectGetWidth(frame) / arrItems.count;
        }else{
            
            iLabelWidth = 70;
        }
        
        UIScrollView *scrollView = nil;
        UIView *view = self;
        CGSize contentSize = CGSizeZero;
        
        if (scrollEnabled) {
            
            scrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
            scrollView.bounces = NO;
            scrollView.showsHorizontalScrollIndicator = NO;
            
            [self addSubview:scrollView];
            
            view = scrollView;
            
            contentSize = CGSizeMake(iPadding + iFirstItemLeadingSpace +  iSpacing, iLabelHeight);
        }
        
        self.dicImageViews = [NSMutableDictionary dictionary];
        self.dicLabels = [NSMutableDictionary dictionary];
        
        for (int i = 0; i < arrItems.count; i ++) {
            
            STCustomToolBarItem *item = arrItems[i];
            
            if (!item.defaultImage && !item.selecedImage && !item.strTitle) {
                
                continue;
            }
            
            if (item.defaultImage || item.selecedImage) {
                
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(iLastMaxX, iPadding, iImageWidth, iImageWidth)];
                imageView.image = i == 0 ? item.selecedImage : item.defaultImage;
                imageView.tag = i + 100;
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onViewTaped:)];
                [imageView addGestureRecognizer:tap];
                
                [view addSubview:imageView];
                
                iLastMaxX += iImageWidth;
                
                [self.dicImageViews setObject:imageView forKey:@(i)];
                
                if (scrollEnabled) {
                    
                    contentSize = CGSizeMake(contentSize.width + iImageWidth + iSpacing, iLabelHeight);
                }
            }
            
            if (item.strTitle) {
                
                UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(iLastMaxX, 0, iLabelWidth, iLabelHeight)];
                label.text = item.strTitle;
                label.textColor = i == 0 ? highlightedColor : [UIColor lightGrayColor];
                label.textAlignment = NSTextAlignmentCenter;
                label.font = [UIFont boldSystemFontOfSize:18.0];
                label.adjustsFontSizeToFitWidth = YES;
                label.tag = i + 1000;
                label.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onViewTaped:)];
                [label addGestureRecognizer:tap];
                
                [view addSubview:label];
                
                iLastMaxX += iLabelWidth;
                
                [self.dicLabels setObject:label forKey:@(i)];
                
                if (scrollEnabled) {
                    
                    contentSize = CGSizeMake(contentSize.width + iLabelWidth, iLabelHeight);
                }
            }
            
            iLastMaxX += iSpacing;
        }
        
        if (scrollEnabled) {
            
            contentSize = CGSizeMake(contentSize.width + iPadding, iLabelHeight);
            scrollView.contentSize = contentSize;
        }
        
        
        int iMarkWidth = 5;
        int iMarkHeight = 5;
        
        self.selectedMark = [[UIView alloc] init];
        self.selectedMark.backgroundColor = [UIColor redColor];
        
        switch (markViewStyle) {
                
            case STCustomToolBarDottedMark:
            {
                self.selectedMark.layer.cornerRadius = iMarkHeight / 2.0f;
            }
                break;
                
                
            case STCustomToolBarSquareMark:
            {
                iMarkWidth = iImageWidth / 2;
            }
                break;
                
            default:
                break;
        }
        
        [self.selectedMark setFrame:CGRectMake(0, 0, iMarkWidth, iMarkHeight)];
        
        int iCenterX = iPadding + iFirstItemLeadingSpace;
        STCustomToolBarItem *firstItem = arrItems.firstObject;
        
        if (firstItem.defaultImage || firstItem.selecedImage) {
            
            iCenterX += iImageWidth / 2;
            
            if (firstItem.strTitle) {
                
                iCenterX += (iImageWidth / 2 + iLabelWidth / 2);
            }
        }else{
            
            if (firstItem.strTitle) {
                
                iCenterX += iLabelWidth / 2;
            }
        }
        
        self.selectedMark.center = CGPointMake(iCenterX, iLabelHeight - iMarkHeight / 2.0);
        
        [view addSubview:self.selectedMark];
        
        return self;
    }
    
    return nil;
}




- (void)onViewTaped:(UITapGestureRecognizer *)tap
{
    UIImageView *preImageView = [self.dicImageViews objectForKey:@(self.iSelectedIndex)];
    UILabel *preLabel = [self.dicLabels objectForKey:@(self.iSelectedIndex)];
    
    preImageView.image = self.arrItems[self.iSelectedIndex].defaultImage;
    preLabel.textColor = [UIColor lightGrayColor];
    
    int iTag = (int)tap.view.tag;
    int iSelectedIndex = 0;
    
    if ([tap.view isKindOfClass:[UIImageView class]]) {
        
        iSelectedIndex = iTag - 100;
    }else{
        
        iSelectedIndex = iTag - 1000;
    }
    
    UIImageView *currentImageView = [self.dicImageViews objectForKey:@(iSelectedIndex)];
    UILabel *currentLabel = [self.dicLabels objectForKey:@(iSelectedIndex)];
    
    currentImageView.image = self.arrItems[iSelectedIndex].selecedImage;
    currentLabel.textColor = self.highlightedColor;
    
    self.iSelectedIndex = iSelectedIndex;
    
    int iCenterX = 0;
    
    if (currentImageView) {
        
        iCenterX = currentImageView.center.x;
    }
    
    if (currentLabel) {
        
        iCenterX = currentLabel.center.x;
    }
    
    CGPoint center = CGPointMake(iCenterX, CGRectGetHeight(self.frame) - CGRectGetHeight(self.selectedMark.frame) / 2);
    
    [UIView animateWithDuration:0.15 animations:^{
       
        self.selectedMark.center = center;
    }];
    
    if (self.callback) {
        
        self.callback(iSelectedIndex);
    }
}





/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
