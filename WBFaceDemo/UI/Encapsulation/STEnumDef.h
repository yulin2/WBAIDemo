//
//  STEnumDef.h
//  SenseArDemo
//
//  Created by sluin on 2017/9/5.
//  Copyright © 2017年 SenseTime. All rights reserved.
//

#ifndef STEnumDef_h
#define STEnumDef_h

typedef enum : NSUInteger {
    
    notDownload = 0,
    
    isDownloaded,
    
    isDownloading
    
} CollectionViewCellModelDownloadStatus;



#endif /* STEnumDef_h */
